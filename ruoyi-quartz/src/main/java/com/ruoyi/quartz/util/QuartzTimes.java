package com.ruoyi.quartz.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class QuartzTimes {
    /**
     * 获取昨天时间
     * @return
     */
    public static String gettoday(){
        Calendar calendar = Calendar.getInstance();

        Date time = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");  // 设置日期格式
        String strTime = simpleDateFormat.format(time);
        return strTime;
    }

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date time = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");  // 设置日期格式
        String strTime = simpleDateFormat.format(time);
        System.out.println(strTime);
    }
}
