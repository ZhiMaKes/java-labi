package com.ruoyi.quartz.task;

import com.ruoyi.classrecords.domain.ClassRecords;
import com.ruoyi.classrecords.service.IClassRecordsService;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.course.domain.Course;
import com.ruoyi.course.service.ICourseService;
import com.ruoyi.order.domain.Order;
import com.ruoyi.order.service.IOrderService;
import com.ruoyi.orderdeduct.domain.OrderDeduct;
import com.ruoyi.orderdeduct.service.IOrderDeductService;
import com.ruoyi.quartz.util.QuartzTimes;
import com.ruoyi.scheduling.domain.DzCourseScheduling;
import com.ruoyi.scheduling.service.IDzCourseSchedulingService;
import com.ruoyi.student.domain.DzStudent;
import com.ruoyi.student.service.IDzStudentService;
import com.ruoyi.utils.Times;
import com.ruoyi.withholdingfees.domain.WithholdingFees;
import com.ruoyi.withholdingfees.service.IWithholdingFeesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.List;

import static com.ruoyi.common.utils.uuid.Seq.getId;

/**
 * 大智定时任务
 *
 * @author ruoyi
 */
@Component("DzTask")
public class DzTask {



    /**
     * 每天更新课程表的状态（非待结算，已结算，变成待结算）
     */
    public void UPDATEDzCourseSchedulingDayS() {
        System.out.println("每天更新课程表的状态（非待结算，已结算，变成待结算）");
        dzCourseSchedulingService.UPDATEDzCourseSchedulingDayS();

    }

    /**
     * 每20分钟更新今天这个课程的状态, 未开始，进行中，待结算
     */
    public void UPDATEDzCourseSchedulingState() throws ParseException {
        System.out.println("每20分钟更新今天这个课程的状态, 未开始，进行中，待结算");
        DzCourseScheduling dzCourseScheduling = new DzCourseScheduling();
        dzCourseScheduling.setStudyDate(QuartzTimes.gettoday());

        List<DzCourseScheduling> dzCourseSchedulings = dzCourseSchedulingService.selectDzCourseSchedulingList(dzCourseScheduling);
        for (int i = 0; i < dzCourseSchedulings.size(); i++) {
            String type=  Times.StateType(dzCourseSchedulings.get(i).getStudyDate()+" " +dzCourseSchedulings.get(i).getClassStart(),dzCourseSchedulings.get(i).getStudyDate()+" " +dzCourseSchedulings.get(i).getClassFinish());
//
            if (dzCourseSchedulings.get(i).getState()!="已结算") {
                dzCourseSchedulings.get(i).setState(type);
                dzCourseSchedulingService.updateDzCourseScheduling( dzCourseSchedulings.get(i));
            }


        }


    }
    @Autowired
    private IWithholdingFeesService withholdingFeesService;
    @Autowired
    private IOrderService orderService;
    @Autowired
    private IClassRecordsService classRecordsService;
    @Autowired
    private IDzStudentService dzStudentService;
    @Autowired
    private IOrderDeductService orderDeductService;
    @Autowired
    private ICourseService courseService;
    @Autowired
    public IDzCourseSchedulingService dzCourseSchedulingService;

    public  void  Calculateorders(){
        DzStudent dzStudent=new DzStudent();
        dzStudent.setUserState("在读学员");
        List<DzStudent> list = dzStudentService.selectDzStudentList(dzStudent);
        for (int i=0; i<list.size(); i++) {
            DzStudent dzStudent1 = list.get(i);

            Long id = dzStudent1.getId();
            Order orderquery = new Order();
            orderquery.setStudentId(""+id);

            List<Order> orders = orderService.selectOrderList(orderquery);
            int allvalue=0;
            for (int w=0; w<orders.size(); w++) {
                String residue = orders.get(w).getResidue();
                int intresidue = Integer.parseInt(residue);
                allvalue=allvalue+intresidue;

            }
            dzStudent1.setRemainingClassHours(""+allvalue);
            dzStudentService.updateDzStudent(dzStudent1);



        }
    }
    public void UPDATERemainingClassHours(){
        Calculateorders();
    }



    public void ryParams(String params) {
        System.out.println("执行有参方法：" + params);
    }

    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }


}
