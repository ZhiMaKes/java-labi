package com.ruoyi.expenditure.service;

import java.util.List;
import com.ruoyi.expenditure.domain.Expenditure;

/**
 * 费项Service接口
 * 
 * @author 大智
 * @date 2024-01-11
 */
public interface IExpenditureService 
{
    /**
     * 查询费项
     * 
     * @param id 费项主键
     * @return 费项
     */
    public Expenditure selectExpenditureById(Long id);

    /**
     * 查询费项列表
     * 
     * @param expenditure 费项
     * @return 费项集合
     */
    public List<Expenditure> selectExpenditureList(Expenditure expenditure);

    /**
     * 新增费项
     * 
     * @param expenditure 费项
     * @return 结果
     */
    public int insertExpenditure(Expenditure expenditure);

    /**
     * 修改费项
     * 
     * @param expenditure 费项
     * @return 结果
     */
    public int updateExpenditure(Expenditure expenditure);

    /**
     * 批量删除费项
     * 
     * @param ids 需要删除的费项主键集合
     * @return 结果
     */
    public int deleteExpenditureByIds(Long[] ids);

    /**
     * 删除费项信息
     * 
     * @param id 费项主键
     * @return 结果
     */
    public int deleteExpenditureById(Long id);
}
