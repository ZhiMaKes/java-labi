package com.ruoyi.expenditure.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.expenditure.mapper.ExpenditureMapper;
import com.ruoyi.expenditure.domain.Expenditure;
import com.ruoyi.expenditure.service.IExpenditureService;

/**
 * 费项Service业务层处理
 * 
 * @author 大智
 * @date 2024-01-11
 */
@Service
public class ExpenditureServiceImpl implements IExpenditureService 
{
    @Autowired
    private ExpenditureMapper expenditureMapper;

    /**
     * 查询费项
     * 
     * @param id 费项主键
     * @return 费项
     */
    @Override
    public Expenditure selectExpenditureById(Long id)
    {
        return expenditureMapper.selectExpenditureById(id);
    }

    /**
     * 查询费项列表
     * 
     * @param expenditure 费项
     * @return 费项
     */
    @Override
    public List<Expenditure> selectExpenditureList(Expenditure expenditure)
    {
        return expenditureMapper.selectExpenditureList(expenditure);
    }

    /**
     * 新增费项
     * 
     * @param expenditure 费项
     * @return 结果
     */
    @Override
    public int insertExpenditure(Expenditure expenditure)
    {
        expenditure.setCreateTime(DateUtils.getNowDate());
        return expenditureMapper.insertExpenditure(expenditure);
    }

    /**
     * 修改费项
     * 
     * @param expenditure 费项
     * @return 结果
     */
    @Override
    public int updateExpenditure(Expenditure expenditure)
    {
        expenditure.setUpdateTime(DateUtils.getNowDate());
        return expenditureMapper.updateExpenditure(expenditure);
    }

    /**
     * 批量删除费项
     * 
     * @param ids 需要删除的费项主键
     * @return 结果
     */
    @Override
    public int deleteExpenditureByIds(Long[] ids)
    {
        return expenditureMapper.deleteExpenditureByIds(ids);
    }

    /**
     * 删除费项信息
     * 
     * @param id 费项主键
     * @return 结果
     */
    @Override
    public int deleteExpenditureById(Long id)
    {
        return expenditureMapper.deleteExpenditureById(id);
    }
}
