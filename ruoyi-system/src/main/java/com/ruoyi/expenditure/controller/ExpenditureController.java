package com.ruoyi.expenditure.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.expenditure.domain.Expenditure;
import com.ruoyi.expenditure.service.IExpenditureService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 费项Controller
 * 
 * @author 大智
 * @date 2024-01-11
 */
@RestController
@RequestMapping("/expenditure/expenditure")
public class ExpenditureController extends BaseController
{
    @Autowired
    private IExpenditureService expenditureService;

    /**
     * 查询费项列表
     */
    @PreAuthorize("@ss.hasPermi('expenditure:expenditure:list')")
    @GetMapping("/list")
    public TableDataInfo list(Expenditure expenditure)
    {
        startPage();
        List<Expenditure> list = expenditureService.selectExpenditureList(expenditure);
        return getDataTable(list);
    }

    /**
     * 导出费项列表
     */
    @PreAuthorize("@ss.hasPermi('expenditure:expenditure:export')")
    @Log(title = "费项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Expenditure expenditure)
    {
        List<Expenditure> list = expenditureService.selectExpenditureList(expenditure);
        ExcelUtil<Expenditure> util = new ExcelUtil<Expenditure>(Expenditure.class);
        util.exportExcel(response, list, "费项数据");
    }

    /**
     * 获取费项详细信息
     */
    @PreAuthorize("@ss.hasPermi('expenditure:expenditure:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(expenditureService.selectExpenditureById(id));
    }

    /**
     * 新增费项
     */
    @PreAuthorize("@ss.hasPermi('expenditure:expenditure:add')")
    @Log(title = "费项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Expenditure expenditure)
    {
        return toAjax(expenditureService.insertExpenditure(expenditure));
    }

    /**
     * 修改费项
     */
    @PreAuthorize("@ss.hasPermi('expenditure:expenditure:edit')")
    @Log(title = "费项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Expenditure expenditure)
    {
        return toAjax(expenditureService.updateExpenditure(expenditure));
    }

    /**
     * 删除费项
     */
    @PreAuthorize("@ss.hasPermi('expenditure:expenditure:remove')")
    @Log(title = "费项", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(expenditureService.deleteExpenditureByIds(ids));
    }
}
