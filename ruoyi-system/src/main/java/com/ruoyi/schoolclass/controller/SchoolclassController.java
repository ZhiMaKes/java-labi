package com.ruoyi.schoolclass.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.scheduling.domain.DzCourseScheduling;
import com.ruoyi.scheduling.service.IDzCourseSchedulingService;
import com.ruoyi.student.domain.DzStudent;
import com.ruoyi.student.service.IDzStudentService;
import com.ruoyi.utils.ArrayUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.schoolclass.domain.Schoolclass;
import com.ruoyi.schoolclass.service.ISchoolclassService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 班级Controller
 *
 * @author ruoyi
 * @date 2024-01-12
 */
@RestController
@RequestMapping("/schoolclass/schoolclass")
public class SchoolclassController extends BaseController {
    @Autowired
    private ISchoolclassService schoolclassService;
    @Autowired
    private IDzStudentService dzStudentService;
    @Autowired
    public IDzCourseSchedulingService dzCourseSchedulingService;

    /**
     * 查询班级列表
     */
    @PreAuthorize("@ss.hasPermi('schoolclass:schoolclass:list')")
    @GetMapping("/list")
    public TableDataInfo list(Schoolclass schoolclass) {
        startPage();
        List<Schoolclass> list = schoolclassService.selectSchoolclassList(schoolclass);
        for (int i = 0; i < list.size(); i++) {
            String classStudent = list.get(i).getClassStudent();
            //设置学生列表
            if (classStudent != null && classStudent != "") {
                String[] arr = classStudent.split(",");
                List<DzStudent> dzStudentByIds = dzStudentService.getDzStudentByIds(arr);
                list.get(i).setStudentlist(dzStudentByIds);
            }


        }


        return getDataTable(list);
    }

    /**
     * 导出班级列表
     */
    @PreAuthorize("@ss.hasPermi('schoolclass:schoolclass:export')")
    @Log(title = "班级", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Schoolclass schoolclass) {
        List<Schoolclass> list = schoolclassService.selectSchoolclassList(schoolclass);
        ExcelUtil<Schoolclass> util = new ExcelUtil<Schoolclass>(Schoolclass.class);
        util.exportExcel(response, list, "班级数据");
    }

    /**
     * 获取班级详细信息
     */
    @PreAuthorize("@ss.hasPermi('schoolclass:schoolclass:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        Schoolclass schoolclass = schoolclassService.selectSchoolclassById(id);

        String classStudent = schoolclass.getClassStudent();
        //设置学生列表
        if (classStudent != null && classStudent != "") {
            String[] arr = classStudent.split(",");
            List<DzStudent> dzStudentByIds = dzStudentService.getDzStudentByIds(arr);
            schoolclass.setStudentlist(dzStudentByIds);
        }


        return success(schoolclass);
    }

    /**
     * 新增班级
     */
    @PreAuthorize("@ss.hasPermi('schoolclass:schoolclass:add')")
    @Log(title = "班级", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Schoolclass schoolclass) {
        return toAjax(schoolclassService.insertSchoolclass(schoolclass));
    }

    /**
     * 修改班级
     */
    @PreAuthorize("@ss.hasPermi('schoolclass:schoolclass:edit')")
    @Log(title = "班级", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Schoolclass schoolclass) {
        //传输的值
        String remark10 = schoolclass.getRemark10();
        if (remark10 != null) {
            schoolclass.setRemark10(null);
            Schoolclass schoolclass1 = schoolclassService.selectSchoolclassById(schoolclass.getId());
            String classStudent = schoolclass1.getClassStudent();
            System.out.println(classStudent);
            if (classStudent != null && classStudent.length() > 0) {

                String[] result = classStudent.split(",");
                if (ArrayUtils.findStr(result, remark10)) {
                    return error("此课程已存在该学员");
                } else {
                    String[] strings = ArrayUtils.ArrayAddOne(result, remark10);
                    String join = StringUtils.join(strings, ",");


                    schoolclass.setClassStudent(join);
                    schoolclassService.updateSchoolclass(schoolclass);

                    //更新排课表里的内容（未完成）
                    DzCourseScheduling dzCourseScheduling = new DzCourseScheduling();

                    dzCourseScheduling.setDeptId(schoolclass.getDeptId());
//                    dzCourseScheduling.set(schoolclass.getClassName());
                    dzCourseScheduling.setState("未开始");

                    List<DzCourseScheduling> listDzCourseScheduling = dzCourseSchedulingService.selectDzCourseSchedulingList(dzCourseScheduling);

                    for (int i = 0; i < listDzCourseScheduling.size(); i++) {
                        listDzCourseScheduling.get(i).setClassStudent(join);

                    }
                    return success("添加成功");
                }


            } else {

                //添加一个新的进入

                schoolclass.setClassStudent(remark10);

                schoolclassService.updateSchoolclass(schoolclass);

                //更新排课表里的内容（未完成）
//                List<DzCourseScheduling> listDzCourseScheduling = dzCourseSchedulingService.selectDzCourseSchedulingList(dzCourseScheduling);

//                for (int i = 0; i < listDzCourseScheduling.size(); i++) {
//                    listDzCourseScheduling.get(i).setClassStudent(remark10);
//
//                }

                return success("添加成功");
            }


        } else {
            return toAjax(schoolclassService.updateSchoolclass(schoolclass));
        }


    }

    /**
     * 删除班级
     */
    @PreAuthorize("@ss.hasPermi('schoolclass:schoolclass:remove')")
    @Log(title = "班级", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(schoolclassService.deleteSchoolclassByIds(ids));
    }
}
