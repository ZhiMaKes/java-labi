package com.ruoyi.schoolclass.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.schoolclass.mapper.SchoolclassMapper;
import com.ruoyi.schoolclass.domain.Schoolclass;
import com.ruoyi.schoolclass.service.ISchoolclassService;

/**
 * 班级Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
@Service
public class SchoolclassServiceImpl implements ISchoolclassService 
{
    @Autowired
    private SchoolclassMapper schoolclassMapper;

    /**
     * 查询班级
     * 
     * @param id 班级主键
     * @return 班级
     */
    @Override
    public Schoolclass selectSchoolclassById(Long id)
    {
        return schoolclassMapper.selectSchoolclassById(id);
    }

    /**
     * 查询班级列表
     * 
     * @param schoolclass 班级
     * @return 班级
     */
    @Override
    public List<Schoolclass> selectSchoolclassList(Schoolclass schoolclass)
    {
        return schoolclassMapper.selectSchoolclassList(schoolclass);
    }

    /**
     * 新增班级
     * 
     * @param schoolclass 班级
     * @return 结果
     */
    @Override
    public int insertSchoolclass(Schoolclass schoolclass)
    {
        schoolclass.setCreateTime(DateUtils.getNowDate());
        return schoolclassMapper.insertSchoolclass(schoolclass);
    }

    /**
     * 修改班级
     * 
     * @param schoolclass 班级
     * @return 结果
     */
    @Override
    public int updateSchoolclass(Schoolclass schoolclass)
    {
        schoolclass.setUpdateTime(DateUtils.getNowDate());
        return schoolclassMapper.updateSchoolclass(schoolclass);
    }

    /**
     * 批量删除班级
     * 
     * @param ids 需要删除的班级主键
     * @return 结果
     */
    @Override
    public int deleteSchoolclassByIds(Long[] ids)
    {
        return schoolclassMapper.deleteSchoolclassByIds(ids);
    }

    /**
     * 删除班级信息
     * 
     * @param id 班级主键
     * @return 结果
     */
    @Override
    public int deleteSchoolclassById(Long id)
    {
        return schoolclassMapper.deleteSchoolclassById(id);
    }
}
