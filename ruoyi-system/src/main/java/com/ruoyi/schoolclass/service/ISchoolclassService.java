package com.ruoyi.schoolclass.service;

import java.util.List;
import com.ruoyi.schoolclass.domain.Schoolclass;

/**
 * 班级Service接口
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
public interface ISchoolclassService 
{
    /**
     * 查询班级
     * 
     * @param id 班级主键
     * @return 班级
     */
    public Schoolclass selectSchoolclassById(Long id);

    /**
     * 查询班级列表
     * 
     * @param schoolclass 班级
     * @return 班级集合
     */
    public List<Schoolclass> selectSchoolclassList(Schoolclass schoolclass);

    /**
     * 新增班级
     * 
     * @param schoolclass 班级
     * @return 结果
     */
    public int insertSchoolclass(Schoolclass schoolclass);

    /**
     * 修改班级
     * 
     * @param schoolclass 班级
     * @return 结果
     */
    public int updateSchoolclass(Schoolclass schoolclass);

    /**
     * 批量删除班级
     * 
     * @param ids 需要删除的班级主键集合
     * @return 结果
     */
    public int deleteSchoolclassByIds(Long[] ids);

    /**
     * 删除班级信息
     * 
     * @param id 班级主键
     * @return 结果
     */
    public int deleteSchoolclassById(Long id);
}
