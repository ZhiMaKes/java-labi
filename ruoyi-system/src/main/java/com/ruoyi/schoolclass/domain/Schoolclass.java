package com.ruoyi.schoolclass.domain;

import com.ruoyi.student.domain.DzStudent;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 班级对象 dz_schoolclass
 * 
 * @author ruoyi
 * @date 2024-01-12
 */
public class Schoolclass extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    private String label;
    /** id */
    private Long id;

    /** 机构ID */
    @Excel(name = "机构ID")
    private String deptId;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 关联课程 */
    @Excel(name = "关联课程")
    private String classCourse;

    /** 班级容量 */
    @Excel(name = "班级容量")
    private String classCapacity;

    /** 请假默认计课时 */
    @Excel(name = "请假默认计课时")
    private String classAskforleave;

    /** 未到默认计课时 */
    @Excel(name = "未到默认计课时")
    private String classNoyet;

    /** 周几排课 */
    @Excel(name = "周几排课")
    private String classWorkOut;

    /** 循环方式 */
    @Excel(name = "循环方式")
    private String classCirculate;

    /** 上课时间开始 */
    @Excel(name = "上课时间开始")
    private String classStart;

    /** 上课时间结束 */
    @Excel(name = "上课时间结束")
    private String classFinish;

    /** 课时数 */
    @Excel(name = "课时数")
    private String classHours;

    /** 授课老师 */
    @Excel(name = "授课老师")
    private String classTeacher;

    /** 考勤员 */
    @Excel(name = "考勤员")
    private String classAttendant;

    /** 上课教师 */
    @Excel(name = "上课教师")
    private String classRoom;

    /** 学员列表 */
    @Excel(name = "学员列表")
    private String classStudent;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 备注2 */
    @Excel(name = "备注2")
    private String remark2;

    /** 备注3 */
    @Excel(name = "备注3")
    private String remark3;

    /** 备注4 */
    @Excel(name = "备注4")
    private String remark4;

    /** 备注5 */
    @Excel(name = "备注5")
    private String remark5;

    /** 备注6 */
    @Excel(name = "备注6")
    private String remark6;

    /** 备注7 */
    @Excel(name = "备注7")
    private String remark7;

    /** 备注8 */
    @Excel(name = "备注8")
    private String remark8;

    /** 备注9 */
    @Excel(name = "备注9")
    private String remark9;

    /** 备注10 */
    @Excel(name = "备注10")
    private String remark10;

    public List<DzStudent> getStudentlist() {
        return studentlist;
    }

    public void setStudentlist(List<DzStudent> studentlist) {
        this.studentlist = studentlist;
    }

    private List<DzStudent > studentlist;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDeptId(String deptId) 
    {
        this.deptId = deptId;
    }

    public String getDeptId() 
    {
        return deptId;
    }
    public void setClassName(String className) 
    {
        this.className = className;
    }

    public String getClassName() 
    {
        return className;
    }
    public void setClassCourse(String classCourse) 
    {
        this.classCourse = classCourse;
    }

    public String getClassCourse() 
    {
        return classCourse;
    }
    public void setClassCapacity(String classCapacity) 
    {
        this.classCapacity = classCapacity;
    }

    public String getClassCapacity() 
    {
        return classCapacity;
    }
    public void setClassAskforleave(String classAskforleave) 
    {
        this.classAskforleave = classAskforleave;
    }

    public String getClassAskforleave() 
    {
        return classAskforleave;
    }
    public void setClassNoyet(String classNoyet) 
    {
        this.classNoyet = classNoyet;
    }

    public String getClassNoyet() 
    {
        return classNoyet;
    }
    public void setClassWorkOut(String classWorkOut) 
    {
        this.classWorkOut = classWorkOut;
    }

    public String getClassWorkOut() 
    {
        return classWorkOut;
    }
    public void setClassCirculate(String classCirculate) 
    {
        this.classCirculate = classCirculate;
    }

    public String getClassCirculate() 
    {
        return classCirculate;
    }
    public void setClassStart(String classStart) 
    {
        this.classStart = classStart;
    }

    public String getClassStart() 
    {
        return classStart;
    }
    public void setClassFinish(String classFinish) 
    {
        this.classFinish = classFinish;
    }

    public String getClassFinish() 
    {
        return classFinish;
    }
    public void setClassHours(String classHours) 
    {
        this.classHours = classHours;
    }

    public String getClassHours() 
    {
        return classHours;
    }
    public void setClassTeacher(String classTeacher) 
    {
        this.classTeacher = classTeacher;
    }

    public String getClassTeacher() 
    {
        return classTeacher;
    }
    public void setClassAttendant(String classAttendant) 
    {
        this.classAttendant = classAttendant;
    }

    public String getClassAttendant() 
    {
        return classAttendant;
    }
    public void setClassRoom(String classRoom) 
    {
        this.classRoom = classRoom;
    }

    public String getClassRoom() 
    {
        return classRoom;
    }
    public void setClassStudent(String classStudent) 
    {
        this.classStudent = classStudent;
    }

    public String getClassStudent() 
    {
        return classStudent;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setRemark2(String remark2) 
    {
        this.remark2 = remark2;
    }

    public String getRemark2() 
    {
        return remark2;
    }
    public void setRemark3(String remark3) 
    {
        this.remark3 = remark3;
    }

    public String getRemark3() 
    {
        return remark3;
    }
    public void setRemark4(String remark4) 
    {
        this.remark4 = remark4;
    }

    public String getRemark4() 
    {
        return remark4;
    }
    public void setRemark5(String remark5) 
    {
        this.remark5 = remark5;
    }

    public String getRemark5() 
    {
        return remark5;
    }
    public void setRemark6(String remark6) 
    {
        this.remark6 = remark6;
    }

    public String getRemark6() 
    {
        return remark6;
    }
    public void setRemark7(String remark7) 
    {
        this.remark7 = remark7;
    }

    public String getRemark7() 
    {
        return remark7;
    }
    public void setRemark8(String remark8) 
    {
        this.remark8 = remark8;
    }

    public String getRemark8() 
    {
        return remark8;
    }
    public void setRemark9(String remark9) 
    {
        this.remark9 = remark9;
    }

    public String getRemark9() 
    {
        return remark9;
    }
    public void setRemark10(String remark10) 
    {
        this.remark10 = remark10;
    }

    public String getRemark10() 
    {
        return remark10;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("deptId", getDeptId())
            .append("className", getClassName())
            .append("classCourse", getClassCourse())
            .append("classCapacity", getClassCapacity())
            .append("classAskforleave", getClassAskforleave())
            .append("classNoyet", getClassNoyet())
            .append("classWorkOut", getClassWorkOut())
            .append("classCirculate", getClassCirculate())
            .append("classStart", getClassStart())
            .append("classFinish", getClassFinish())
            .append("classHours", getClassHours())
            .append("classTeacher", getClassTeacher())
            .append("classAttendant", getClassAttendant())
            .append("classRoom", getClassRoom())
            .append("classStudent", getClassStudent())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("remark2", getRemark2())
            .append("remark3", getRemark3())
            .append("remark4", getRemark4())
            .append("remark5", getRemark5())
            .append("remark6", getRemark6())
            .append("remark7", getRemark7())
            .append("remark8", getRemark8())
            .append("remark9", getRemark9())
            .append("remark10", getRemark10())
            .toString();
    }
}
