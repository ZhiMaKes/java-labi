package com.ruoyi.withholdingfees.service;

import java.util.List;
import com.ruoyi.withholdingfees.domain.WithholdingFees;

/**
 * 待扣Service接口
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
public interface IWithholdingFeesService 
{
    /**
     * 查询待扣
     * 
     * @param id 待扣主键
     * @return 待扣
     */
    public WithholdingFees selectWithholdingFeesById(Long id);

    /**
     * 查询待扣列表
     * 
     * @param withholdingFees 待扣
     * @return 待扣集合
     */
    public List<WithholdingFees> selectWithholdingFeesList(WithholdingFees withholdingFees);

    /**
     * 新增待扣
     * 
     * @param withholdingFees 待扣
     * @return 结果
     */
    public int insertWithholdingFees(WithholdingFees withholdingFees);

    /**
     * 修改待扣
     * 
     * @param withholdingFees 待扣
     * @return 结果
     */
    public int updateWithholdingFees(WithholdingFees withholdingFees);

    /**
     * 批量删除待扣
     * 
     * @param ids 需要删除的待扣主键集合
     * @return 结果
     */
    public int deleteWithholdingFeesByIds(Long[] ids);

    /**
     * 删除待扣信息
     * 
     * @param id 待扣主键
     * @return 结果
     */
    public int deleteWithholdingFeesById(Long id);
}
