package com.ruoyi.withholdingfees.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.withholdingfees.mapper.WithholdingFeesMapper;
import com.ruoyi.withholdingfees.domain.WithholdingFees;
import com.ruoyi.withholdingfees.service.IWithholdingFeesService;

/**
 * 待扣Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
@Service
public class WithholdingFeesServiceImpl implements IWithholdingFeesService 
{
    @Autowired
    private WithholdingFeesMapper withholdingFeesMapper;

    /**
     * 查询待扣
     * 
     * @param id 待扣主键
     * @return 待扣
     */
    @Override
    public WithholdingFees selectWithholdingFeesById(Long id)
    {
        return withholdingFeesMapper.selectWithholdingFeesById(id);
    }

    /**
     * 查询待扣列表
     * 
     * @param withholdingFees 待扣
     * @return 待扣
     */
    @Override
    public List<WithholdingFees> selectWithholdingFeesList(WithholdingFees withholdingFees)
    {
        return withholdingFeesMapper.selectWithholdingFeesList(withholdingFees);
    }

    /**
     * 新增待扣
     * 
     * @param withholdingFees 待扣
     * @return 结果
     */
    @Override
    public int insertWithholdingFees(WithholdingFees withholdingFees)
    {
        return withholdingFeesMapper.insertWithholdingFees(withholdingFees);
    }

    /**
     * 修改待扣
     * 
     * @param withholdingFees 待扣
     * @return 结果
     */
    @Override
    public int updateWithholdingFees(WithholdingFees withholdingFees)
    {
        return withholdingFeesMapper.updateWithholdingFees(withholdingFees);
    }

    /**
     * 批量删除待扣
     * 
     * @param ids 需要删除的待扣主键
     * @return 结果
     */
    @Override
    public int deleteWithholdingFeesByIds(Long[] ids)
    {
        return withholdingFeesMapper.deleteWithholdingFeesByIds(ids);
    }

    /**
     * 删除待扣信息
     * 
     * @param id 待扣主键
     * @return 结果
     */
    @Override
    public int deleteWithholdingFeesById(Long id)
    {
        return withholdingFeesMapper.deleteWithholdingFeesById(id);
    }
}
