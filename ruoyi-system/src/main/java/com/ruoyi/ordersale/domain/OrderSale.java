package com.ruoyi.ordersale.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单销售对象 dz_order_sale
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public class OrderSale extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long orderSaleId;

    /** 订单id */
    @Excel(name = "订单id")
    private String orderId;

    /** 销售总额 */
    @Excel(name = "销售总额")
    private String orderSaleAllmoney;

    /** 销售业绩老师 */
    @Excel(name = "销售业绩老师")
    private String orderSaleTeacher;

    /** 销售业绩比例 */
    @Excel(name = "销售业绩比例")
    private String orderSaleProportion;

    /** 销售业绩金额 */
    @Excel(name = "销售业绩金额")
    private String orderSaleMoney;

    /** 机构ID */
    @Excel(name = "机构ID")
    private String deptId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 备注2 */
    @Excel(name = "备注2")
    private String remark2;

    /** 备注3 */
    @Excel(name = "备注3")
    private String remark3;

    /** 备注4 */
    @Excel(name = "备注4")
    private String remark4;

    /** 备注5 */
    @Excel(name = "备注5")
    private String remark5;

    /** 备注6 */
    @Excel(name = "备注6")
    private String remark6;

    /** 备注7 */
    @Excel(name = "备注7")
    private String remark7;

    /** 备注8 */
    @Excel(name = "备注8")
    private String remark8;

    /** 备注9 */
    @Excel(name = "备注9")
    private String remark9;

    /** 备注10 */
    @Excel(name = "备注10")
    private String remark10;

    public void setOrderSaleId(Long orderSaleId) 
    {
        this.orderSaleId = orderSaleId;
    }

    public Long getOrderSaleId() 
    {
        return orderSaleId;
    }
    public void setOrderId(String orderId) 
    {
        this.orderId = orderId;
    }

    public String getOrderId() 
    {
        return orderId;
    }
    public void setOrderSaleAllmoney(String orderSaleAllmoney) 
    {
        this.orderSaleAllmoney = orderSaleAllmoney;
    }

    public String getOrderSaleAllmoney() 
    {
        return orderSaleAllmoney;
    }
    public void setOrderSaleTeacher(String orderSaleTeacher) 
    {
        this.orderSaleTeacher = orderSaleTeacher;
    }

    public String getOrderSaleTeacher() 
    {
        return orderSaleTeacher;
    }
    public void setOrderSaleProportion(String orderSaleProportion) 
    {
        this.orderSaleProportion = orderSaleProportion;
    }

    public String getOrderSaleProportion() 
    {
        return orderSaleProportion;
    }
    public void setOrderSaleMoney(String orderSaleMoney) 
    {
        this.orderSaleMoney = orderSaleMoney;
    }

    public String getOrderSaleMoney() 
    {
        return orderSaleMoney;
    }
    public void setDeptId(String deptId) 
    {
        this.deptId = deptId;
    }

    public String getDeptId() 
    {
        return deptId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setRemark2(String remark2) 
    {
        this.remark2 = remark2;
    }

    public String getRemark2() 
    {
        return remark2;
    }
    public void setRemark3(String remark3) 
    {
        this.remark3 = remark3;
    }

    public String getRemark3() 
    {
        return remark3;
    }
    public void setRemark4(String remark4) 
    {
        this.remark4 = remark4;
    }

    public String getRemark4() 
    {
        return remark4;
    }
    public void setRemark5(String remark5) 
    {
        this.remark5 = remark5;
    }

    public String getRemark5() 
    {
        return remark5;
    }
    public void setRemark6(String remark6) 
    {
        this.remark6 = remark6;
    }

    public String getRemark6() 
    {
        return remark6;
    }
    public void setRemark7(String remark7) 
    {
        this.remark7 = remark7;
    }

    public String getRemark7() 
    {
        return remark7;
    }
    public void setRemark8(String remark8) 
    {
        this.remark8 = remark8;
    }

    public String getRemark8() 
    {
        return remark8;
    }
    public void setRemark9(String remark9) 
    {
        this.remark9 = remark9;
    }

    public String getRemark9() 
    {
        return remark9;
    }
    public void setRemark10(String remark10) 
    {
        this.remark10 = remark10;
    }

    public String getRemark10() 
    {
        return remark10;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("orderSaleId", getOrderSaleId())
            .append("orderId", getOrderId())
            .append("orderSaleAllmoney", getOrderSaleAllmoney())
            .append("orderSaleTeacher", getOrderSaleTeacher())
            .append("orderSaleProportion", getOrderSaleProportion())
            .append("orderSaleMoney", getOrderSaleMoney())
            .append("deptId", getDeptId())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("remark2", getRemark2())
            .append("remark3", getRemark3())
            .append("remark4", getRemark4())
            .append("remark5", getRemark5())
            .append("remark6", getRemark6())
            .append("remark7", getRemark7())
            .append("remark8", getRemark8())
            .append("remark9", getRemark9())
            .append("remark10", getRemark10())
            .toString();
    }
}
