package com.ruoyi.ordersale.service;

import java.util.List;
import com.ruoyi.ordersale.domain.OrderSale;

/**
 * 订单销售Service接口
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IOrderSaleService 
{
    /**
     * 查询订单销售
     * 
     * @param orderSaleId 订单销售主键
     * @return 订单销售
     */
    public OrderSale selectOrderSaleByOrderSaleId(Long orderSaleId);

    /**
     * 查询订单销售列表
     * 
     * @param orderSale 订单销售
     * @return 订单销售集合
     */
    public List<OrderSale> selectOrderSaleList(OrderSale orderSale);

    /**
     * 新增订单销售
     * 
     * @param orderSale 订单销售
     * @return 结果
     */
    public int insertOrderSale(OrderSale orderSale);

    /**
     * 修改订单销售
     * 
     * @param orderSale 订单销售
     * @return 结果
     */
    public int updateOrderSale(OrderSale orderSale);

    /**
     * 批量删除订单销售
     * 
     * @param orderSaleIds 需要删除的订单销售主键集合
     * @return 结果
     */
    public int deleteOrderSaleByOrderSaleIds(Long[] orderSaleIds);

    /**
     * 删除订单销售信息
     * 
     * @param orderSaleId 订单销售主键
     * @return 结果
     */
    public int deleteOrderSaleByOrderSaleId(Long orderSaleId);
}
