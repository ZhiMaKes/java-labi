package com.ruoyi.ordersale.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ordersale.mapper.OrderSaleMapper;
import com.ruoyi.ordersale.domain.OrderSale;
import com.ruoyi.ordersale.service.IOrderSaleService;

/**
 * 订单销售Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
@Service
public class OrderSaleServiceImpl implements IOrderSaleService 
{
    @Autowired
    private OrderSaleMapper orderSaleMapper;

    /**
     * 查询订单销售
     * 
     * @param orderSaleId 订单销售主键
     * @return 订单销售
     */
    @Override
    public OrderSale selectOrderSaleByOrderSaleId(Long orderSaleId)
    {
        return orderSaleMapper.selectOrderSaleByOrderSaleId(orderSaleId);
    }

    /**
     * 查询订单销售列表
     * 
     * @param orderSale 订单销售
     * @return 订单销售
     */
    @Override
    public List<OrderSale> selectOrderSaleList(OrderSale orderSale)
    {
        return orderSaleMapper.selectOrderSaleList(orderSale);
    }

    /**
     * 新增订单销售
     * 
     * @param orderSale 订单销售
     * @return 结果
     */
    @Override
    public int insertOrderSale(OrderSale orderSale)
    {
        orderSale.setCreateTime(DateUtils.getNowDate());
        return orderSaleMapper.insertOrderSale(orderSale);
    }

    /**
     * 修改订单销售
     * 
     * @param orderSale 订单销售
     * @return 结果
     */
    @Override
    public int updateOrderSale(OrderSale orderSale)
    {
        orderSale.setUpdateTime(DateUtils.getNowDate());
        return orderSaleMapper.updateOrderSale(orderSale);
    }

    /**
     * 批量删除订单销售
     * 
     * @param orderSaleIds 需要删除的订单销售主键
     * @return 结果
     */
    @Override
    public int deleteOrderSaleByOrderSaleIds(Long[] orderSaleIds)
    {
        return orderSaleMapper.deleteOrderSaleByOrderSaleIds(orderSaleIds);
    }

    /**
     * 删除订单销售信息
     * 
     * @param orderSaleId 订单销售主键
     * @return 结果
     */
    @Override
    public int deleteOrderSaleByOrderSaleId(Long orderSaleId)
    {
        return orderSaleMapper.deleteOrderSaleByOrderSaleId(orderSaleId);
    }
}
