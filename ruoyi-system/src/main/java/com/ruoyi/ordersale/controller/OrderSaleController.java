package com.ruoyi.ordersale.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.ordersale.domain.OrderSale;
import com.ruoyi.ordersale.service.IOrderSaleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单销售Controller
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
@RestController
@RequestMapping("/ordersale/ordersale")
public class OrderSaleController extends BaseController
{
    @Autowired
    private IOrderSaleService orderSaleService;

    /**
     * 查询订单销售列表
     */
    @PreAuthorize("@ss.hasPermi('ordersale:ordersale:list')")
    @GetMapping("/list")
    public TableDataInfo list(OrderSale orderSale)
    {
        startPage();
        List<OrderSale> list = orderSaleService.selectOrderSaleList(orderSale);
        return getDataTable(list);
    }

    /**
     * 导出订单销售列表
     */
    @PreAuthorize("@ss.hasPermi('ordersale:ordersale:export')")
    @Log(title = "订单销售", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OrderSale orderSale)
    {
        List<OrderSale> list = orderSaleService.selectOrderSaleList(orderSale);
        ExcelUtil<OrderSale> util = new ExcelUtil<OrderSale>(OrderSale.class);
        util.exportExcel(response, list, "订单销售数据");
    }

    /**
     * 获取订单销售详细信息
     */
    @PreAuthorize("@ss.hasPermi('ordersale:ordersale:query')")
    @GetMapping(value = "/{orderSaleId}")
    public AjaxResult getInfo(@PathVariable("orderSaleId") Long orderSaleId)
    {
        return success(orderSaleService.selectOrderSaleByOrderSaleId(orderSaleId));
    }

    /**
     * 新增订单销售
     */
    @PreAuthorize("@ss.hasPermi('ordersale:ordersale:add')")
    @Log(title = "订单销售", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OrderSale orderSale)
    {
        return toAjax(orderSaleService.insertOrderSale(orderSale));
    }

    /**
     * 修改订单销售
     */
    @PreAuthorize("@ss.hasPermi('ordersale:ordersale:edit')")
    @Log(title = "订单销售", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OrderSale orderSale)
    {
        return toAjax(orderSaleService.updateOrderSale(orderSale));
    }

    /**
     * 删除订单销售
     */
    @PreAuthorize("@ss.hasPermi('ordersale:ordersale:remove')")
    @Log(title = "订单销售", businessType = BusinessType.DELETE)
	@DeleteMapping("/{orderSaleIds}")
    public AjaxResult remove(@PathVariable Long[] orderSaleIds)
    {
        return toAjax(orderSaleService.deleteOrderSaleByOrderSaleIds(orderSaleIds));
    }
}
