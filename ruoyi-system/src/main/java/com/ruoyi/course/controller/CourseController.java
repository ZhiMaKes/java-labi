package com.ruoyi.course.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.order.domain.Order;
import com.ruoyi.order.service.IOrderService;
import com.ruoyi.schoolclass.domain.Schoolclass;
import com.ruoyi.schoolclass.service.ISchoolclassService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.course.domain.Course;
import com.ruoyi.course.service.ICourseService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程管理Controller
 * 
 * @author 大智
 * @date 2024-01-11
 */
@RestController
@RequestMapping("/course/course")
public class CourseController extends BaseController
{
    @Autowired
    private ICourseService courseService;
    @Autowired
    private ISchoolclassService schoolclassService;
    @Autowired
    private IOrderService orderService;
    /**
     * 查询课程管理列表
     */
    @PreAuthorize("@ss.hasPermi('course:course:list')")
    @GetMapping("/list")
    public TableDataInfo list(Course course)
    {

        List<Course> list = courseService.selectCourseList(course);
        for (int i=0; i<list.size(); i++) {
            //课程名称
            String courseName = list.get(i).getCourseName();
            Order order=new Order();
            order.setOrderCourse(courseName);
            order.setDeptId(course.getDeptId());

            List<Order> ordersize = orderService.selectOrderListByCourse(order);
            list.get(i).setCourseNowStudent(""+ordersize.size());


        }
        return getDataTable(list);
    }

    /**
     * 查询课程管理列表
     */
    @PreAuthorize("@ss.hasPermi('course:course:list')")
    @GetMapping("/course_and_class")
    public TableDataInfo course_and_class(Course course)
    {

        List<Course> list = courseService.selectCourseList(course);

        Schoolclass schoolclass=new Schoolclass();
        schoolclass.setDeptId(course.getDeptId());
        List<Schoolclass> schoolclasseslist = schoolclassService.selectSchoolclassList(schoolclass);
        if (list!=null&&list.size()>0) {
            for (int i=0; i<list.size(); i++) {

                list.get(i).setLabel( list.get(i).getCourseName());
                list.get(i).setValue(list.get(i).getCourseName());
                List<Schoolclass > ccList=new ArrayList<Schoolclass>();
                if (schoolclasseslist!=null&&schoolclasseslist.size()>0) {
                    for (int w=0; w<schoolclasseslist.size(); w++) {

                        schoolclasseslist.get(w).setLabel( schoolclasseslist.get(w).getClassName());
                        schoolclasseslist.get(w).setValue(schoolclasseslist.get(w).getClassName());

                        if (list.get(i).getCourseName()!=null&&list.get(i).getCourseName().equals(schoolclasseslist.get(w).getClassCourse())) {
                            ccList.add(schoolclasseslist.get(w));

                        }

                    }
                    list.get(i).setChildren(ccList);
                }


            }
        }



        return getDataTable(list);
    }


    /**
     * 导出课程管理列表
     */
    @PreAuthorize("@ss.hasPermi('course:course:export')")
    @Log(title = "课程管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Course course)
    {
        List<Course> list = courseService.selectCourseList(course);
        ExcelUtil<Course> util = new ExcelUtil<Course>(Course.class);
        util.exportExcel(response, list, "课程管理数据");
    }

    /**
     * 获取课程管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('course:course:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(courseService.selectCourseById(id));
    }

    /**
     * 新增课程管理
     */
    @PreAuthorize("@ss.hasPermi('course:course:add')")
    @Log(title = "课程管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Course course)



    {
        String courseName = course.getCourseName();


        Course course1=new Course();
        course1.setCourseName(courseName);
        course1.setDeptId(course.getDeptId());

        List<Course> courses = courseService.selectCourseList(course1);
        if (courses.size()>0) {
            return error("存在同名称的课程，请重新输入！");
        }  else{
            return toAjax(courseService.insertCourse(course));
        }




    }

    /**
     * 修改课程管理
     */
    @PreAuthorize("@ss.hasPermi('course:course:edit')")
    @Log(title = "课程管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Course course)
    {
        return toAjax(courseService.updateCourse(course));
    }

    /**
     * 删除课程管理
     */
    @PreAuthorize("@ss.hasPermi('course:course:remove')")
    @Log(title = "课程管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(courseService.deleteCourseByIds(ids));
    }
}
