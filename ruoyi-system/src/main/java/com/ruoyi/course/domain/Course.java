package com.ruoyi.course.domain;

import com.ruoyi.schoolclass.domain.Schoolclass;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 课程管理对象 dz_course
 * 
 * @author 大智
 * @date 2024-01-11
 */
public class Course extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    private String label;
    /** 课程编号 */
    @Excel(name = "课程编号")
    private String courseId;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String courseName;

    /** 课程科目 */
    @Excel(name = "课程科目")
    private String courseSubject;

    /** 授课模式 */
    @Excel(name = "授课模式")
    private String courseTeachingMode;

    /** 自动节课 */
    @Excel(name = "自动节课")
    private String courseAutoFinish;

    /** 课程状态（在开课程/历史课程） */
    @Excel(name = "课程状态")
    private String courseState;

    /** 在读学员 */
    @Excel(name = "在读学员")
    private String courseNowStudent;

    /** 历史学员 */
    @Excel(name = "历史学员")
    private String courseOverStudent;

    /** 机构ID */
    @Excel(name = "机构ID")
    private String deptId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 备注2 */
    @Excel(name = "备注2")
    private String remark2;

    /** 备注3 */
    @Excel(name = "备注3")
    private String remark3;

    /** 备注4 */
    @Excel(name = "备注4")
    private String remark4;

    /** 备注5 */
    @Excel(name = "备注5")
    private String remark5;

    /** 备注6 */
    @Excel(name = "备注6")
    private String remark6;

    /** 备注7 */
    @Excel(name = "备注7")
    private String remark7;

    /** 备注8 */
    @Excel(name = "备注8")
    private String remark8;

    /** 备注9 */
    @Excel(name = "备注9")
    private String remark9;

    /** 备注10 */
    @Excel(name = "备注10")
    private String remark10;


    public List<Schoolclass> getChildren() {
        return children;
    }

    public void setChildren(List<Schoolclass> children) {
        this.children = children;
    }

    public List<Schoolclass> children;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCourseId(String courseId) 
    {
        this.courseId = courseId;
    }

    public String getCourseId() 
    {
        return courseId;
    }
    public void setCourseName(String courseName) 
    {
        this.courseName = courseName;
    }

    public String getCourseName() 
    {
        return courseName;
    }
    public void setCourseSubject(String courseSubject) 
    {
        this.courseSubject = courseSubject;
    }

    public String getCourseSubject() 
    {
        return courseSubject;
    }
    public void setCourseTeachingMode(String courseTeachingMode) 
    {
        this.courseTeachingMode = courseTeachingMode;
    }

    public String getCourseTeachingMode() 
    {
        return courseTeachingMode;
    }
    public void setCourseAutoFinish(String courseAutoFinish) 
    {
        this.courseAutoFinish = courseAutoFinish;
    }

    public String getCourseAutoFinish() 
    {
        return courseAutoFinish;
    }
    public void setCourseState(String courseState) 
    {
        this.courseState = courseState;
    }

    public String getCourseState() 
    {
        return courseState;
    }
    public void setCourseNowStudent(String courseNowStudent) 
    {
        this.courseNowStudent = courseNowStudent;
    }

    public String getCourseNowStudent() 
    {
        return courseNowStudent;
    }
    public void setCourseOverStudent(String courseOverStudent) 
    {
        this.courseOverStudent = courseOverStudent;
    }

    public String getCourseOverStudent() 
    {
        return courseOverStudent;
    }
    public void setDeptId(String deptId) 
    {
        this.deptId = deptId;
    }

    public String getDeptId() 
    {
        return deptId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setRemark2(String remark2) 
    {
        this.remark2 = remark2;
    }

    public String getRemark2() 
    {
        return remark2;
    }
    public void setRemark3(String remark3) 
    {
        this.remark3 = remark3;
    }

    public String getRemark3() 
    {
        return remark3;
    }
    public void setRemark4(String remark4) 
    {
        this.remark4 = remark4;
    }

    public String getRemark4() 
    {
        return remark4;
    }
    public void setRemark5(String remark5) 
    {
        this.remark5 = remark5;
    }

    public String getRemark5() 
    {
        return remark5;
    }
    public void setRemark6(String remark6) 
    {
        this.remark6 = remark6;
    }

    public String getRemark6() 
    {
        return remark6;
    }
    public void setRemark7(String remark7) 
    {
        this.remark7 = remark7;
    }

    public String getRemark7() 
    {
        return remark7;
    }
    public void setRemark8(String remark8) 
    {
        this.remark8 = remark8;
    }

    public String getRemark8() 
    {
        return remark8;
    }
    public void setRemark9(String remark9) 
    {
        this.remark9 = remark9;
    }

    public String getRemark9() 
    {
        return remark9;
    }
    public void setRemark10(String remark10) 
    {
        this.remark10 = remark10;
    }

    public String getRemark10() 
    {
        return remark10;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("courseId", getCourseId())
            .append("courseName", getCourseName())
            .append("courseSubject", getCourseSubject())
            .append("courseTeachingMode", getCourseTeachingMode())
            .append("courseAutoFinish", getCourseAutoFinish())
            .append("courseState", getCourseState())
            .append("courseNowStudent", getCourseNowStudent())
            .append("courseOverStudent", getCourseOverStudent())
            .append("deptId", getDeptId())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("remark2", getRemark2())
            .append("remark3", getRemark3())
            .append("remark4", getRemark4())
            .append("remark5", getRemark5())
            .append("remark6", getRemark6())
            .append("remark7", getRemark7())
            .append("remark8", getRemark8())
            .append("remark9", getRemark9())
            .append("remark10", getRemark10())
            .toString();
    }
}
