package com.ruoyi.orderdeduct.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.orderdeduct.domain.OrderDeduct;
import com.ruoyi.orderdeduct.service.IOrderDeductService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单消费Controller
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
@RestController
@RequestMapping("/orderdeduct/orderdeduct")
public class OrderDeductController extends BaseController
{
    @Autowired
    private IOrderDeductService orderDeductService;

    /**
     * 查询订单消费列表
     */
    @PreAuthorize("@ss.hasPermi('orderdeduct:orderdeduct:list')")
    @GetMapping("/list")
    public TableDataInfo list(OrderDeduct orderDeduct)
    {
        startPage();
        List<OrderDeduct> list = orderDeductService.selectOrderDeductList(orderDeduct);
        return getDataTable(list);
    }

    /**
     * 导出订单消费列表
     */
    @PreAuthorize("@ss.hasPermi('orderdeduct:orderdeduct:export')")
    @Log(title = "订单消费", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OrderDeduct orderDeduct)
    {
        List<OrderDeduct> list = orderDeductService.selectOrderDeductList(orderDeduct);
        ExcelUtil<OrderDeduct> util = new ExcelUtil<OrderDeduct>(OrderDeduct.class);
        util.exportExcel(response, list, "订单消费数据");
    }

    /**
     * 获取订单消费详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderdeduct:orderdeduct:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(orderDeductService.selectOrderDeductById(id));
    }

    /**
     * 新增订单消费
     */
    @PreAuthorize("@ss.hasPermi('orderdeduct:orderdeduct:add')")
    @Log(title = "订单消费", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OrderDeduct orderDeduct)
    {
        return toAjax(orderDeductService.insertOrderDeduct(orderDeduct));
    }

    /**
     * 修改订单消费
     */
    @PreAuthorize("@ss.hasPermi('orderdeduct:orderdeduct:edit')")
    @Log(title = "订单消费", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OrderDeduct orderDeduct)
    {
        return toAjax(orderDeductService.updateOrderDeduct(orderDeduct));
    }

    /**
     * 删除订单消费
     */
    @PreAuthorize("@ss.hasPermi('orderdeduct:orderdeduct:remove')")
    @Log(title = "订单消费", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(orderDeductService.deleteOrderDeductByIds(ids));
    }
}
