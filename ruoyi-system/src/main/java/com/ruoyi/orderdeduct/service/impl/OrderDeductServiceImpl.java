package com.ruoyi.orderdeduct.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.orderdeduct.mapper.OrderDeductMapper;
import com.ruoyi.orderdeduct.domain.OrderDeduct;
import com.ruoyi.orderdeduct.service.IOrderDeductService;

/**
 * 订单消费Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
@Service
public class OrderDeductServiceImpl implements IOrderDeductService 
{
    @Autowired
    private OrderDeductMapper orderDeductMapper;

    /**
     * 查询订单消费
     * 
     * @param id 订单消费主键
     * @return 订单消费
     */
    @Override
    public OrderDeduct selectOrderDeductById(Long id)
    {
        return orderDeductMapper.selectOrderDeductById(id);
    }

    /**
     * 查询订单消费列表
     * 
     * @param orderDeduct 订单消费
     * @return 订单消费
     */
    @Override
    public List<OrderDeduct> selectOrderDeductList(OrderDeduct orderDeduct)
    {
        return orderDeductMapper.selectOrderDeductList(orderDeduct);
    }

    /**
     * 新增订单消费
     * 
     * @param orderDeduct 订单消费
     * @return 结果
     */
    @Override
    public int insertOrderDeduct(OrderDeduct orderDeduct)
    {
        return orderDeductMapper.insertOrderDeduct(orderDeduct);
    }

    /**
     * 修改订单消费
     * 
     * @param orderDeduct 订单消费
     * @return 结果
     */
    @Override
    public int updateOrderDeduct(OrderDeduct orderDeduct)
    {
        return orderDeductMapper.updateOrderDeduct(orderDeduct);
    }

    /**
     * 批量删除订单消费
     * 
     * @param ids 需要删除的订单消费主键
     * @return 结果
     */
    @Override
    public int deleteOrderDeductByIds(Long[] ids)
    {
        return orderDeductMapper.deleteOrderDeductByIds(ids);
    }

    /**
     * 删除订单消费信息
     * 
     * @param id 订单消费主键
     * @return 结果
     */
    @Override
    public int deleteOrderDeductById(Long id)
    {
        return orderDeductMapper.deleteOrderDeductById(id);
    }
}
