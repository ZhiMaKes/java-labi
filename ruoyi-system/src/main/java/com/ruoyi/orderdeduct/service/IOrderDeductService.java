package com.ruoyi.orderdeduct.service;

import java.util.List;
import com.ruoyi.orderdeduct.domain.OrderDeduct;

/**
 * 订单消费Service接口
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
public interface IOrderDeductService 
{
    /**
     * 查询订单消费
     * 
     * @param id 订单消费主键
     * @return 订单消费
     */
    public OrderDeduct selectOrderDeductById(Long id);

    /**
     * 查询订单消费列表
     * 
     * @param orderDeduct 订单消费
     * @return 订单消费集合
     */
    public List<OrderDeduct> selectOrderDeductList(OrderDeduct orderDeduct);

    /**
     * 新增订单消费
     * 
     * @param orderDeduct 订单消费
     * @return 结果
     */
    public int insertOrderDeduct(OrderDeduct orderDeduct);

    /**
     * 修改订单消费
     * 
     * @param orderDeduct 订单消费
     * @return 结果
     */
    public int updateOrderDeduct(OrderDeduct orderDeduct);

    /**
     * 批量删除订单消费
     * 
     * @param ids 需要删除的订单消费主键集合
     * @return 结果
     */
    public int deleteOrderDeductByIds(Long[] ids);

    /**
     * 删除订单消费信息
     * 
     * @param id 订单消费主键
     * @return 结果
     */
    public int deleteOrderDeductById(Long id);
}
