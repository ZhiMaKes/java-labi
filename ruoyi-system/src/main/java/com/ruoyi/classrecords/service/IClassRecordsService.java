package com.ruoyi.classrecords.service;

import java.util.List;
import com.ruoyi.classrecords.domain.ClassRecords;

/**
 * 上课记录Service接口
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
public interface IClassRecordsService 
{
    /**
     * 查询上课记录
     * 
     * @param id 上课记录主键
     * @return 上课记录
     */
    public ClassRecords selectClassRecordsById(Long id);

    /**
     * 查询上课记录列表
     * 
     * @param classRecords 上课记录
     * @return 上课记录集合
     */
    public List<ClassRecords> selectClassRecordsList(ClassRecords classRecords);

    /**
     * 新增上课记录
     * 
     * @param classRecords 上课记录
     * @return 结果
     */
    public int insertClassRecords(ClassRecords classRecords);

    /**
     * 修改上课记录
     * 
     * @param classRecords 上课记录
     * @return 结果
     */
    public int updateClassRecords(ClassRecords classRecords);

    /**
     * 批量删除上课记录
     * 
     * @param ids 需要删除的上课记录主键集合
     * @return 结果
     */
    public int deleteClassRecordsByIds(Long[] ids);

    /**
     * 删除上课记录信息
     * 
     * @param id 上课记录主键
     * @return 结果
     */
    public int deleteClassRecordsById(Long id);
}
