package com.ruoyi.classrecords.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.classrecords.mapper.ClassRecordsMapper;
import com.ruoyi.classrecords.domain.ClassRecords;
import com.ruoyi.classrecords.service.IClassRecordsService;

/**
 * 上课记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
@Service
public class ClassRecordsServiceImpl implements IClassRecordsService 
{
    @Autowired
    private ClassRecordsMapper classRecordsMapper;

    /**
     * 查询上课记录
     * 
     * @param id 上课记录主键
     * @return 上课记录
     */
    @Override
    public ClassRecords selectClassRecordsById(Long id)
    {
        return classRecordsMapper.selectClassRecordsById(id);
    }

    /**
     * 查询上课记录列表
     * 
     * @param classRecords 上课记录
     * @return 上课记录
     */
    @Override
    public List<ClassRecords> selectClassRecordsList(ClassRecords classRecords)
    {
        return classRecordsMapper.selectClassRecordsList(classRecords);
    }

    /**
     * 新增上课记录
     * 
     * @param classRecords 上课记录
     * @return 结果
     */
    @Override
    public int insertClassRecords(ClassRecords classRecords)
    {
        classRecords.setCreateTime(DateUtils.getNowDate());
        return classRecordsMapper.insertClassRecords(classRecords);
    }

    /**
     * 修改上课记录
     * 
     * @param classRecords 上课记录
     * @return 结果
     */
    @Override
    public int updateClassRecords(ClassRecords classRecords)
    {
        classRecords.setUpdateTime(DateUtils.getNowDate());
        return classRecordsMapper.updateClassRecords(classRecords);
    }

    /**
     * 批量删除上课记录
     * 
     * @param ids 需要删除的上课记录主键
     * @return 结果
     */
    @Override
    public int deleteClassRecordsByIds(Long[] ids)
    {
        return classRecordsMapper.deleteClassRecordsByIds(ids);
    }

    /**
     * 删除上课记录信息
     * 
     * @param id 上课记录主键
     * @return 结果
     */
    @Override
    public int deleteClassRecordsById(Long id)
    {
        return classRecordsMapper.deleteClassRecordsById(id);
    }
}
