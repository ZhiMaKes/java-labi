package com.ruoyi.classrecords.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 上课记录对象 dz_class_records
 *
 * @author ruoyi
 * @date 2024-02-05
 */
public class ClassRecords extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 排课ID */
    @Excel(name = "排课ID")
    private Long scheduleId;

    /** 机构ID */
    @Excel(name = "机构ID")
    private String deptId;

    /** 学生ID */
    @Excel(name = "学生ID")
    private String studentId;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String studentName;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String linkPhone;

    /** 剩余课时 */
    @Excel(name = "剩余课时")
    private String remainingclasshours;

    /** 上课日期 */
    @Excel(name = "上课日期")
    private String date;

    /** 上课时间 */
    @Excel(name = "上课时间")
    private String time;

    /** 课程ID */
    @Excel(name = "课程ID")
    private String courseId;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String courseName;

    /** 班级ID */
    @Excel(name = "班级ID")
    private String classId;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 授课老师 */
    @Excel(name = "授课老师")
    private String teacher;

    /** 考勤老师 */
    @Excel(name = "考勤老师")
    private String attendanceTeacher;

    /** 课消数 */
    @Excel(name = "课消数")
    private String courseCancellations;

    /** 考勤状态（未开始，到课，未到，已到） */
    @Excel(name = "考勤状态", readConverterExp = "到课，请假，已到")
    private String attendanceState;

    /** 签到时间 */
    @Excel(name = "签到时间")
    private String singnTime;

    /** 结算状态（未结算,已结算） */
    @Excel(name = "结算状态", readConverterExp = "未=结算,已结算")
    private String settlestatus;

    /** 出席提醒 */
    @Excel(name = "出席提醒")
    private String attendremind;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 备注2 */
    @Excel(name = "备注2")
    private String remark2;

    /** 备注3 */
    @Excel(name = "备注3")
    private String remark3;

    /** 备注4 */
    @Excel(name = "备注4")
    private String remark4;

    /** 备注5 */
    @Excel(name = "备注5")
    private String remark5;

    /** 备注6 */
    @Excel(name = "备注6")
    private String remark6;

    /** 备注7 */
    @Excel(name = "备注7")
    private String remark7;

    /** 备注8 */
    @Excel(name = "备注8")
    private String remark8;

    /** 备注9 */
    @Excel(name = "备注9")
    private String remark9;

    /** 备注10 */
    @Excel(name = "备注10")
    private String remark10;


    public String getCommunicate_key() {
        return communicate_key;
    }

    public void setCommunicate_key(String communicate_key) {
        this.communicate_key = communicate_key;
    }
    /** 通讯key */
    private String communicate_key;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setScheduleId(Long scheduleId)
    {
        this.scheduleId = scheduleId;
    }

    public Long getScheduleId()
    {
        return scheduleId;
    }
    public void setDeptId(String deptId)
    {
        this.deptId = deptId;
    }

    public String getDeptId()
    {
        return deptId;
    }
    public void setStudentId(String studentId)
    {
        this.studentId = studentId;
    }

    public String getStudentId()
    {
        return studentId;
    }
    public void setStudentName(String studentName)
    {
        this.studentName = studentName;
    }

    public String getStudentName()
    {
        return studentName;
    }
    public void setLinkPhone(String linkPhone)
    {
        this.linkPhone = linkPhone;
    }

    public String getLinkPhone()
    {
        return linkPhone;
    }
    public void setRemainingclasshours(String remainingclasshours)
    {
        this.remainingclasshours = remainingclasshours;
    }

    public String getRemainingclasshours()
    {
        return remainingclasshours;
    }
    public void setDate(String date)
    {
        this.date = date;
    }

    public String getDate()
    {
        return date;
    }
    public void setTime(String time)
    {
        this.time = time;
    }

    public String getTime()
    {
        return time;
    }
    public void setCourseId(String courseId)
    {
        this.courseId = courseId;
    }

    public String getCourseId()
    {
        return courseId;
    }
    public void setCourseName(String courseName)
    {
        this.courseName = courseName;
    }

    public String getCourseName()
    {
        return courseName;
    }
    public void setClassId(String classId)
    {
        this.classId = classId;
    }

    public String getClassId()
    {
        return classId;
    }
    public void setClassName(String className)
    {
        this.className = className;
    }

    public String getClassName()
    {
        return className;
    }
    public void setTeacher(String teacher)
    {
        this.teacher = teacher;
    }

    public String getTeacher()
    {
        return teacher;
    }
    public void setAttendanceTeacher(String attendanceTeacher)
    {
        this.attendanceTeacher = attendanceTeacher;
    }

    public String getAttendanceTeacher()
    {
        return attendanceTeacher;
    }
    public void setCourseCancellations(String courseCancellations)
    {
        this.courseCancellations = courseCancellations;
    }

    public String getCourseCancellations()
    {
        return courseCancellations;
    }
    public void setAttendanceState(String attendanceState)
    {
        this.attendanceState = attendanceState;
    }

    public String getAttendanceState()
    {
        return attendanceState;
    }
    public void setSingnTime(String singnTime)
    {
        this.singnTime = singnTime;
    }

    public String getSingnTime()
    {
        return singnTime;
    }
    public void setSettlestatus(String settlestatus)
    {
        this.settlestatus = settlestatus;
    }

    public String getSettlestatus()
    {
        return settlestatus;
    }
    public void setAttendremind(String attendremind)
    {
        this.attendremind = attendremind;
    }

    public String getAttendremind()
    {
        return attendremind;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setRemark2(String remark2)
    {
        this.remark2 = remark2;
    }

    public String getRemark2()
    {
        return remark2;
    }
    public void setRemark3(String remark3)
    {
        this.remark3 = remark3;
    }

    public String getRemark3()
    {
        return remark3;
    }
    public void setRemark4(String remark4)
    {
        this.remark4 = remark4;
    }

    public String getRemark4()
    {
        return remark4;
    }
    public void setRemark5(String remark5)
    {
        this.remark5 = remark5;
    }

    public String getRemark5()
    {
        return remark5;
    }
    public void setRemark6(String remark6)
    {
        this.remark6 = remark6;
    }

    public String getRemark6()
    {
        return remark6;
    }
    public void setRemark7(String remark7)
    {
        this.remark7 = remark7;
    }

    public String getRemark7()
    {
        return remark7;
    }
    public void setRemark8(String remark8)
    {
        this.remark8 = remark8;
    }

    public String getRemark8()
    {
        return remark8;
    }
    public void setRemark9(String remark9)
    {
        this.remark9 = remark9;
    }

    public String getRemark9()
    {
        return remark9;
    }
    public void setRemark10(String remark10)
    {
        this.remark10 = remark10;
    }

    public String getRemark10()
    {
        return remark10;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("scheduleId", getScheduleId())
                .append("deptId", getDeptId())
                .append("studentId", getStudentId())
                .append("studentName", getStudentName())
                .append("linkPhone", getLinkPhone())
                .append("remainingclasshours", getRemainingclasshours())
                .append("date", getDate())
                .append("time", getTime())
                .append("courseId", getCourseId())
                .append("courseName", getCourseName())
                .append("classId", getClassId())
                .append("className", getClassName())
                .append("teacher", getTeacher())
                .append("attendanceTeacher", getAttendanceTeacher())
                .append("courseCancellations", getCourseCancellations())
                .append("attendanceState", getAttendanceState())
                .append("singnTime", getSingnTime())
                .append("settlestatus", getSettlestatus())
                .append("attendremind", getAttendremind())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .append("remark2", getRemark2())
                .append("remark3", getRemark3())
                .append("remark4", getRemark4())
                .append("remark5", getRemark5())
                .append("remark6", getRemark6())
                .append("remark7", getRemark7())
                .append("remark8", getRemark8())
                .append("remark9", getRemark9())
                .append("remark10", getRemark10())
                .toString();
    }
}
