package com.ruoyi.student.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学生信息对象 dz_student
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public class DzStudent extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 学号 */
    @Excel(name = "学号")
    private String studentId;

    /** 状态（在读，不读） */
    @Excel(name = "状态")
    private String userState;

    /** 学生名称 */
    @Excel(name = "学生名称")
    private String userName;

    /** 学生性别 */
    @Excel(name = "学生性别")
    private String sex;

    /** 学生生日 */
    @Excel(name = "学生生日")
    private String birthday;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String phonenumber;

    /** 关系 */
    @Excel(name = "关系")
    private String relationship;

    /** 就读学校 */
    @Excel(name = "就读学校")
    private String school;

    /** 年级 */
    @Excel(name = "年级")
    private String grade;

    /** 住址 */
    @Excel(name = "住址")
    private String address;

    /** 招生渠道 */
    @Excel(name = "招生渠道")
    private String enrollmentChannels;

    /** 跟进人 */
    @Excel(name = "跟进人")
    private String followUpPerson;

    /** 用户账号 */
//    @Excel(name = "用户账号")
    private String userAccount;

    /** 密码 */
//    @Excel(name = "密码")
    private String password;

    /** 帐号状态（0正常 1停用） */
//    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 机构ID */
//    @Excel(name = "机构ID")
    private String deptId;

    /** 班级ID */
//    @Excel(name = "班级ID")
    private String classId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 头像地址 */
//    @Excel(name = "头像地址")
    private String avatar;

    /** 最后登录IP */
//    @Excel(name = "最后登录IP")
    private String loginIp;

    /** 最后登录时间 */
//    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date loginDate;

    /** 总课时 */
    @Excel(name = "总课时")
    private String totalClassHours;

    /** 剩余课时 */
    @Excel(name = "剩余课时")
    private String remainingClassHours;

    /** 累计学习课时 */
    @Excel(name = "累计学习课时")
    private String accumulatedLearningHours;

    /** 标签 */
    @Excel(name = "标签")
    private String label;

    /** 备注2 */
//    @Excel(name = "备注2")
    private String remark2;

    /** 备注3 */
//    @Excel(name = "备注3")
    private String remark3;

    /** 备注4 */
//    @Excel(name = "备注4")
    private String remark4;

    /** 备注5 */
//    @Excel(name = "备注5")
    private String remark5;

    /** 备注6 */
//    @Excel(name = "备注6")
    private String remark6;

    /** 备注7 */
//    @Excel(name = "备注7")
    private String remark7;

    /** 备注8 */
//    @Excel(name = "备注8")
    private String remark8;

    /** 备注9 */
//    @Excel(name = "备注9")
    private String remark9;

    /** 备注10 */
//    @Excel(name = "备注10")
    private String remark10;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStudentId(String studentId) 
    {
        this.studentId = studentId;
    }

    public String getStudentId() 
    {
        return studentId;
    }
    public void setUserState(String userState) 
    {
        this.userState = userState;
    }

    public String getUserState() 
    {
        return userState;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setSex(String sex) 
    {
        this.sex = sex;
    }

    public String getSex() 
    {
        return sex;
    }
    public void setBirthday(String birthday) 
    {
        this.birthday = birthday;
    }

    public String getBirthday() 
    {
        return birthday;
    }
    public void setPhonenumber(String phonenumber) 
    {
        this.phonenumber = phonenumber;
    }

    public String getPhonenumber() 
    {
        return phonenumber;
    }
    public void setRelationship(String relationship) 
    {
        this.relationship = relationship;
    }

    public String getRelationship() 
    {
        return relationship;
    }
    public void setSchool(String school) 
    {
        this.school = school;
    }

    public String getSchool() 
    {
        return school;
    }
    public void setGrade(String grade) 
    {
        this.grade = grade;
    }

    public String getGrade() 
    {
        return grade;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setEnrollmentChannels(String enrollmentChannels) 
    {
        this.enrollmentChannels = enrollmentChannels;
    }

    public String getEnrollmentChannels() 
    {
        return enrollmentChannels;
    }
    public void setFollowUpPerson(String followUpPerson) 
    {
        this.followUpPerson = followUpPerson;
    }

    public String getFollowUpPerson() 
    {
        return followUpPerson;
    }
    public void setUserAccount(String userAccount) 
    {
        this.userAccount = userAccount;
    }

    public String getUserAccount() 
    {
        return userAccount;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDeptId(String deptId) 
    {
        this.deptId = deptId;
    }

    public String getDeptId() 
    {
        return deptId;
    }
    public void setClassId(String classId) 
    {
        this.classId = classId;
    }

    public String getClassId() 
    {
        return classId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setAvatar(String avatar) 
    {
        this.avatar = avatar;
    }

    public String getAvatar() 
    {
        return avatar;
    }
    public void setLoginIp(String loginIp) 
    {
        this.loginIp = loginIp;
    }

    public String getLoginIp() 
    {
        return loginIp;
    }
    public void setLoginDate(Date loginDate) 
    {
        this.loginDate = loginDate;
    }

    public Date getLoginDate() 
    {
        return loginDate;
    }
    public void setTotalClassHours(String totalClassHours) 
    {
        this.totalClassHours = totalClassHours;
    }

    public String getTotalClassHours() 
    {
        return totalClassHours;
    }
    public void setRemainingClassHours(String remainingClassHours) 
    {
        this.remainingClassHours = remainingClassHours;
    }

    public String getRemainingClassHours() 
    {
        return remainingClassHours;
    }
    public void setAccumulatedLearningHours(String accumulatedLearningHours) 
    {
        this.accumulatedLearningHours = accumulatedLearningHours;
    }

    public String getAccumulatedLearningHours() 
    {
        return accumulatedLearningHours;
    }
    public void setLabel(String label) 
    {
        this.label = label;
    }

    public String getLabel() 
    {
        return label;
    }
    public void setRemark2(String remark2) 
    {
        this.remark2 = remark2;
    }

    public String getRemark2() 
    {
        return remark2;
    }
    public void setRemark3(String remark3) 
    {
        this.remark3 = remark3;
    }

    public String getRemark3() 
    {
        return remark3;
    }
    public void setRemark4(String remark4) 
    {
        this.remark4 = remark4;
    }

    public String getRemark4() 
    {
        return remark4;
    }
    public void setRemark5(String remark5) 
    {
        this.remark5 = remark5;
    }

    public String getRemark5() 
    {
        return remark5;
    }
    public void setRemark6(String remark6) 
    {
        this.remark6 = remark6;
    }

    public String getRemark6() 
    {
        return remark6;
    }
    public void setRemark7(String remark7) 
    {
        this.remark7 = remark7;
    }

    public String getRemark7() 
    {
        return remark7;
    }
    public void setRemark8(String remark8) 
    {
        this.remark8 = remark8;
    }

    public String getRemark8() 
    {
        return remark8;
    }
    public void setRemark9(String remark9) 
    {
        this.remark9 = remark9;
    }

    public String getRemark9() 
    {
        return remark9;
    }
    public void setRemark10(String remark10) 
    {
        this.remark10 = remark10;
    }

    public String getRemark10() 
    {
        return remark10;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentId", getStudentId())
            .append("userState", getUserState())
            .append("userName", getUserName())
            .append("sex", getSex())
            .append("birthday", getBirthday())
            .append("phonenumber", getPhonenumber())
            .append("relationship", getRelationship())
            .append("school", getSchool())
            .append("grade", getGrade())
            .append("address", getAddress())
            .append("enrollmentChannels", getEnrollmentChannels())
            .append("followUpPerson", getFollowUpPerson())
            .append("userAccount", getUserAccount())
            .append("password", getPassword())
            .append("status", getStatus())
            .append("deptId", getDeptId())
            .append("classId", getClassId())
            .append("delFlag", getDelFlag())
            .append("avatar", getAvatar())
            .append("loginIp", getLoginIp())
            .append("loginDate", getLoginDate())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("totalClassHours", getTotalClassHours())
            .append("remainingClassHours", getRemainingClassHours())
            .append("accumulatedLearningHours", getAccumulatedLearningHours())
            .append("label", getLabel())
            .append("remark", getRemark())
            .append("remark2", getRemark2())
            .append("remark3", getRemark3())
            .append("remark4", getRemark4())
            .append("remark5", getRemark5())
            .append("remark6", getRemark6())
            .append("remark7", getRemark7())
            .append("remark8", getRemark8())
            .append("remark9", getRemark9())
            .append("remark10", getRemark10())
            .toString();
    }
}
