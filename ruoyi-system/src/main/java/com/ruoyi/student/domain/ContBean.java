package com.ruoyi.student.domain;

public class ContBean {
    int now_student_num ;
    int older_student_num ;
    int potential_student_num ;

    public int getNow_student_num() {
        return now_student_num;
    }

    public void setNow_student_num(int now_student_num) {
        this.now_student_num = now_student_num;
    }

    public int getOlder_student_num() {
        return older_student_num;
    }

    public void setOlder_student_num(int older_student_num) {
        this.older_student_num = older_student_num;
    }

    public int getPotential_student_num() {
        return potential_student_num;
    }

    public void setPotential_student_num(int potential_student_num) {
        this.potential_student_num = potential_student_num;
    }

    public int getBirthday_num() {
        return birthday_num;
    }

    public void setBirthday_num(int birthday_num) {
        this.birthday_num = birthday_num;
    }

    public int getCost_num() {
        return cost_num;
    }

    public void setCost_num(int cost_num) {
        this.cost_num = cost_num;
    }

    int birthday_num ;
    int cost_num  ;
}
