package com.ruoyi.student.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.classrecords.domain.ClassRecords;
import com.ruoyi.classrecords.service.IClassRecordsService;
import com.ruoyi.order.domain.Order;
import com.ruoyi.order.service.IOrderService;
import com.ruoyi.scheduling.domain.DzCourseScheduling;
import com.ruoyi.student.domain.ContBean;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.student.domain.DzStudent;
import com.ruoyi.student.service.IDzStudentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学生信息Controller
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
@RestController
@RequestMapping("/student/student")
public class DzStudentController extends BaseController
{
    @Autowired
    private IDzStudentService dzStudentService;

    /**
     * 查询学生信息列表
     */
    @PreAuthorize("@ss.hasPermi('student:student:list')")
    @GetMapping("/list")
    public TableDataInfo list(DzStudent dzStudent)
    {
//        startPage();
        List<DzStudent> list = dzStudentService.selectDzStudentList(dzStudent);
        return getDataTable(list);
    }
    /**
     * 查询学生信息列表
     */
//    @PreAuthorize("@ss.hasPermi('student:student:list')")
    @GetMapping("/getcount")
    public AjaxResult getcount(DzStudent dzStudent )
    {

        int now_student_num = dzStudentService.getRecordCount("在读学员",dzStudent.getDeptId());
        int older_student_num = dzStudentService.getRecordCount("历史学员",dzStudent.getDeptId());
        int potential_student_num = dzStudentService.getRecordCount("潜在学员",dzStudent.getDeptId());

        int Birthday_num = dzStudentService.getBirthdayCount(  dzStudent.getDeptId());
        int Cost_num  = dzStudentService.getCostCount("欠费代缴" ,dzStudent.getDeptId());

        ContBean  contBean=new ContBean();
        contBean.setNow_student_num(now_student_num);
        contBean.setOlder_student_num(older_student_num);
        contBean.setPotential_student_num(potential_student_num);
        contBean.setBirthday_num(Birthday_num);
        contBean.setCost_num(Cost_num);




        return success(contBean);
    }



    /**
     * 导出学生信息列表
     */
    @PreAuthorize("@ss.hasPermi('student:student:export')")
    @Log(title = "学生信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzStudent dzStudent)
    {
        List<DzStudent> list = dzStudentService.selectDzStudentList(dzStudent);
        ExcelUtil<DzStudent> util = new ExcelUtil<DzStudent>(DzStudent.class);
        util.exportExcel(response, list, "学生信息数据");
    }

    /**
     * 获取学生信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('student:student:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dzStudentService.selectDzStudentById(id));
    }

    /**
     * 新增学生信息
     */
    @PreAuthorize("@ss.hasPermi('student:student:add')")
    @Log(title = "学生信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzStudent dzStudent)
    {
        return toAjax(dzStudentService.insertDzStudent(dzStudent));
    }

    /**
     * 修改学生信息
     */
    @PreAuthorize("@ss.hasPermi('student:student:edit')")
    @Log(title = "学生信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DzStudent dzStudent)
    {
        return toAjax(dzStudentService.updateDzStudent(dzStudent));
    }
    @Autowired
    private IClassRecordsService classRecordsService;
    @Autowired
    private IOrderService orderService;
    /**
     * 删除学生信息
     */
    @PreAuthorize("@ss.hasPermi('student:student:remove')")
    @Log(title = "学生信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)



    {
        for (int i=0; i<ids.length; i++) {
            ClassRecords classRecords=new ClassRecords();
            classRecords.setStudentId(""+ids[i]);
            List<ClassRecords> list = classRecordsService.selectClassRecordsList(classRecords);
            for (int z=0; z<list.size(); z++) {

                classRecordsService.deleteClassRecordsById(list.get(z).getId());
            }



        }
        for (int n=0; n<ids.length; n++) {
            Order order=new Order();
            order.setStudentId(""+ids[n]);
            List<Order> list = orderService.selectOrderList(order);
            for (int w=0; w<list.size(); w++) {

                orderService.deleteOrderByOrderId(list.get(w).getOrderId());
            }



        }


        return toAjax(dzStudentService.deleteDzStudentByIds(ids));
    }
}
