package com.ruoyi.student.mapper;

import java.util.List;
import com.ruoyi.student.domain.DzStudent;
import org.apache.ibatis.annotations.Param;

/**
 * 学生信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public interface DzStudentMapper 
{
    /**
     * 查询学生信息
     * 
     * @param id 学生信息主键
     * @return 学生信息
     */
    public DzStudent selectDzStudentById(Long id);
    public int getRecordCount(@Param("userState") String userState, @Param("deptId") String deptId);
    public int getBirthdayCount( @Param("deptId") String deptId);
    public int getCostCount(@Param("remark")  String remark,@Param("deptId") String deptId);



    /**
     * 查询学生信息列表
     * 
     * @param dzStudent 学生信息
     * @return 学生信息集合
     */
    public List<DzStudent> selectDzStudentList(DzStudent dzStudent);

    /**
     * 新增学生信息
     * 
     * @param dzStudent 学生信息
     * @return 结果
     */
    public int insertDzStudent(DzStudent dzStudent);

    /**
     * 修改学生信息
     * 
     * @param dzStudent 学生信息
     * @return 结果
     */
    public int updateDzStudent(DzStudent dzStudent);

    /**
     * 删除学生信息
     * 
     * @param id 学生信息主键
     * @return 结果
     */
    public int deleteDzStudentById(Long id);

    /**
     * 批量删除学生信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzStudentByIds(Long[] ids);

    public List<DzStudent> getDzStudentByIds(String[] ids);

}
