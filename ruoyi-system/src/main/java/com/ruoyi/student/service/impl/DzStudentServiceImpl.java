package com.ruoyi.student.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.student.mapper.DzStudentMapper;
import com.ruoyi.student.domain.DzStudent;
import com.ruoyi.student.service.IDzStudentService;

/**
 * 学生信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
@Service
public class DzStudentServiceImpl implements IDzStudentService 
{
    @Autowired
    private DzStudentMapper dzStudentMapper;

    /**
     * 查询学生信息
     * 
     * @param id 学生信息主键
     * @return 学生信息
     */
    @Override
    public DzStudent selectDzStudentById(Long id)
    {
        return dzStudentMapper.selectDzStudentById(id);
    }
    @Override
    public int getRecordCount(String st,String dept_id)
    {
        return dzStudentMapper.getRecordCount(st,dept_id);
    }
    @Override
    public int getBirthdayCount( String dept_id )
    {
        return dzStudentMapper.getBirthdayCount(dept_id );
    }

    @Override
    public int getCostCount(String st,String dept_id )
    {
        return dzStudentMapper.getCostCount(st, dept_id);
    }



    /**
     * 查询学生信息列表
     * 
     * @param dzStudent 学生信息
     * @return 学生信息
     */
    @Override
    public List<DzStudent> selectDzStudentList(DzStudent dzStudent)
    {
        return dzStudentMapper.selectDzStudentList(dzStudent);
    }

    /**
     * 新增学生信息
     * 
     * @param dzStudent 学生信息
     * @return 结果
     */
    @Override
    public int insertDzStudent(DzStudent dzStudent)
    {
        dzStudent.setCreateTime(DateUtils.getNowDate());
        return dzStudentMapper.insertDzStudent(dzStudent);
    }

    /**
     * 修改学生信息
     * 
     * @param dzStudent 学生信息
     * @return 结果
     */
    @Override
    public int updateDzStudent(DzStudent dzStudent)
    {
        dzStudent.setUpdateTime(DateUtils.getNowDate());
        return dzStudentMapper.updateDzStudent(dzStudent);
    }

    /**
     * 批量删除学生信息
     * 
     * @param ids 需要删除的学生信息主键
     * @return 结果
     */
    @Override
    public int deleteDzStudentByIds(Long[] ids)
    {
        return dzStudentMapper.deleteDzStudentByIds(ids);
    }

    @Override
    public List<DzStudent> getDzStudentByIds(String[] ids)
    {
        return dzStudentMapper.getDzStudentByIds(ids);
    }

    /**
     * 删除学生信息信息
     * 
     * @param id 学生信息主键
     * @return 结果
     */
    @Override
    public int deleteDzStudentById(Long id)
    {
        return dzStudentMapper.deleteDzStudentById(id);
    }
}
