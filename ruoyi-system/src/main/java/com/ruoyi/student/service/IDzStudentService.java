package com.ruoyi.student.service;

import java.util.List;
import com.ruoyi.student.domain.DzStudent;

/**
 * 学生信息Service接口
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IDzStudentService 
{
    /**
     * 查询学生信息
     * 
     * @param id 学生信息主键
     * @return 学生信息
     */
    public DzStudent selectDzStudentById(Long id);

    public int getRecordCount(String st,String dept_id);
    public int getBirthdayCount(  String dept_id);

    public int getCostCount(String st,String dept_id);
    /**
     * 查询学生信息列表
     * 
     * @param dzStudent 学生信息
     * @return 学生信息集合
     */
    public List<DzStudent> selectDzStudentList(DzStudent dzStudent);

    /**
     * 新增学生信息
     * 
     * @param dzStudent 学生信息
     * @return 结果
     */
    public int insertDzStudent(DzStudent dzStudent);

    /**
     * 修改学生信息
     * 
     * @param dzStudent 学生信息
     * @return 结果
     */
    public int updateDzStudent(DzStudent dzStudent);

    /**
     * 批量删除学生信息
     * 
     * @param ids 需要删除的学生信息主键集合
     * @return 结果
     */
    public int deleteDzStudentByIds(Long[] ids);

    /**
     * 删除学生信息信息
     * 
     * @param id 学生信息主键
     * @return 结果
     */
    public int deleteDzStudentById(Long id);

    public List<DzStudent> getDzStudentByIds(String[] ids);

}
