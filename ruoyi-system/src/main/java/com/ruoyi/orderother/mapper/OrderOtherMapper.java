package com.ruoyi.orderother.mapper;

import java.util.List;
import com.ruoyi.orderother.domain.OrderOther;

/**
 * 订单其他Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public interface OrderOtherMapper 
{
    /**
     * 查询订单其他
     * 
     * @param orderotherId 订单其他主键
     * @return 订单其他
     */
    public OrderOther selectOrderOtherByOrderotherId(Long orderotherId);

    /**
     * 查询订单其他列表
     * 
     * @param orderOther 订单其他
     * @return 订单其他集合
     */
    public List<OrderOther> selectOrderOtherList(OrderOther orderOther);

    /**
     * 新增订单其他
     * 
     * @param orderOther 订单其他
     * @return 结果
     */
    public int insertOrderOther(OrderOther orderOther);

    /**
     * 修改订单其他
     * 
     * @param orderOther 订单其他
     * @return 结果
     */
    public int updateOrderOther(OrderOther orderOther);

    /**
     * 删除订单其他
     * 
     * @param orderotherId 订单其他主键
     * @return 结果
     */
    public int deleteOrderOtherByOrderotherId(Long orderotherId);

    /**
     * 批量删除订单其他
     * 
     * @param orderotherIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderOtherByOrderotherIds(Long[] orderotherIds);
}
