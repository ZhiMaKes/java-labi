package com.ruoyi.orderother.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.orderother.domain.OrderOther;
import com.ruoyi.orderother.service.IOrderOtherService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单其他Controller
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
@RestController
@RequestMapping("/orderother/orderother")
public class OrderOtherController extends BaseController
{
    @Autowired
    private IOrderOtherService orderOtherService;

    /**
     * 查询订单其他列表
     */
    @PreAuthorize("@ss.hasPermi('orderother:orderother:list')")
    @GetMapping("/list")
    public TableDataInfo list(OrderOther orderOther)
    {
        startPage();
        List<OrderOther> list = orderOtherService.selectOrderOtherList(orderOther);
        return getDataTable(list);
    }

    /**
     * 导出订单其他列表
     */
    @PreAuthorize("@ss.hasPermi('orderother:orderother:export')")
    @Log(title = "订单其他", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OrderOther orderOther)
    {
        List<OrderOther> list = orderOtherService.selectOrderOtherList(orderOther);
        ExcelUtil<OrderOther> util = new ExcelUtil<OrderOther>(OrderOther.class);
        util.exportExcel(response, list, "订单其他数据");
    }

    /**
     * 获取订单其他详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderother:orderother:query')")
    @GetMapping(value = "/{orderotherId}")
    public AjaxResult getInfo(@PathVariable("orderotherId") Long orderotherId)
    {
        return success(orderOtherService.selectOrderOtherByOrderotherId(orderotherId));
    }

    /**
     * 新增订单其他
     */
    @PreAuthorize("@ss.hasPermi('orderother:orderother:add')")
    @Log(title = "订单其他", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OrderOther orderOther)
    {
        return toAjax(orderOtherService.insertOrderOther(orderOther));
    }

    /**
     * 修改订单其他
     */
    @PreAuthorize("@ss.hasPermi('orderother:orderother:edit')")
    @Log(title = "订单其他", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OrderOther orderOther)
    {
        return toAjax(orderOtherService.updateOrderOther(orderOther));
    }

    /**
     * 删除订单其他
     */
    @PreAuthorize("@ss.hasPermi('orderother:orderother:remove')")
    @Log(title = "订单其他", businessType = BusinessType.DELETE)
	@DeleteMapping("/{orderotherIds}")
    public AjaxResult remove(@PathVariable Long[] orderotherIds)
    {
        return toAjax(orderOtherService.deleteOrderOtherByOrderotherIds(orderotherIds));
    }
}
