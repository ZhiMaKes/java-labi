package com.ruoyi.orderother.service;

import java.util.List;
import com.ruoyi.orderother.domain.OrderOther;

/**
 * 订单其他Service接口
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IOrderOtherService 
{
    /**
     * 查询订单其他
     * 
     * @param orderotherId 订单其他主键
     * @return 订单其他
     */
    public OrderOther selectOrderOtherByOrderotherId(Long orderotherId);

    /**
     * 查询订单其他列表
     * 
     * @param orderOther 订单其他
     * @return 订单其他集合
     */
    public List<OrderOther> selectOrderOtherList(OrderOther orderOther);

    /**
     * 新增订单其他
     * 
     * @param orderOther 订单其他
     * @return 结果
     */
    public int insertOrderOther(OrderOther orderOther);

    /**
     * 修改订单其他
     * 
     * @param orderOther 订单其他
     * @return 结果
     */
    public int updateOrderOther(OrderOther orderOther);

    /**
     * 批量删除订单其他
     * 
     * @param orderotherIds 需要删除的订单其他主键集合
     * @return 结果
     */
    public int deleteOrderOtherByOrderotherIds(Long[] orderotherIds);

    /**
     * 删除订单其他信息
     * 
     * @param orderotherId 订单其他主键
     * @return 结果
     */
    public int deleteOrderOtherByOrderotherId(Long orderotherId);
}
