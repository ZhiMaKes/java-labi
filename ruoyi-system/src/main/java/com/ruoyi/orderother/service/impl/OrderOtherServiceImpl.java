package com.ruoyi.orderother.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.orderother.mapper.OrderOtherMapper;
import com.ruoyi.orderother.domain.OrderOther;
import com.ruoyi.orderother.service.IOrderOtherService;

/**
 * 订单其他Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
@Service
public class OrderOtherServiceImpl implements IOrderOtherService 
{
    @Autowired
    private OrderOtherMapper orderOtherMapper;

    /**
     * 查询订单其他
     * 
     * @param orderotherId 订单其他主键
     * @return 订单其他
     */
    @Override
    public OrderOther selectOrderOtherByOrderotherId(Long orderotherId)
    {
        return orderOtherMapper.selectOrderOtherByOrderotherId(orderotherId);
    }

    /**
     * 查询订单其他列表
     * 
     * @param orderOther 订单其他
     * @return 订单其他
     */
    @Override
    public List<OrderOther> selectOrderOtherList(OrderOther orderOther)
    {
        return orderOtherMapper.selectOrderOtherList(orderOther);
    }

    /**
     * 新增订单其他
     * 
     * @param orderOther 订单其他
     * @return 结果
     */
    @Override
    public int insertOrderOther(OrderOther orderOther)
    {
        orderOther.setCreateTime(DateUtils.getNowDate());
        return orderOtherMapper.insertOrderOther(orderOther);
    }

    /**
     * 修改订单其他
     * 
     * @param orderOther 订单其他
     * @return 结果
     */
    @Override
    public int updateOrderOther(OrderOther orderOther)
    {
        orderOther.setUpdateTime(DateUtils.getNowDate());
        return orderOtherMapper.updateOrderOther(orderOther);
    }

    /**
     * 批量删除订单其他
     * 
     * @param orderotherIds 需要删除的订单其他主键
     * @return 结果
     */
    @Override
    public int deleteOrderOtherByOrderotherIds(Long[] orderotherIds)
    {
        return orderOtherMapper.deleteOrderOtherByOrderotherIds(orderotherIds);
    }

    /**
     * 删除订单其他信息
     * 
     * @param orderotherId 订单其他主键
     * @return 结果
     */
    @Override
    public int deleteOrderOtherByOrderotherId(Long orderotherId)
    {
        return orderOtherMapper.deleteOrderOtherByOrderotherId(orderotherId);
    }
}
