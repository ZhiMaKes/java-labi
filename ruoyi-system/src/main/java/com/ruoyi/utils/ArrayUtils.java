package com.ruoyi.utils;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayUtils {

    public static void main(String[] args) {


        // 输出结果
//            System.out.println("原始数组：" + Arrays.toString(originalArray));
//            System.out.println("新数组：" + Arrays.toString(newArray));
        String[] originalArray = {"1", "2", "3"};
//            String[] strings = ArrayAddOne(originalArray, "4");
        String[] strings = removeValue(originalArray, "2");


        for (int i = 0; i < strings.length; i++) {
            System.out.println(strings[i]);
        }
    }

    /**
     * 向数组添加一个新值
     *
     * @param old
     * @param newone
     * @return
     */
    public static String[] ArrayAddOne(String[] old, String newone) {


        // 将原始数组转换为 ArrayList
        ArrayList<String> arrayList = new ArrayList<>();
        for (String num : old) {
            arrayList.add(num);
        }

        // 向 ArrayList 中添加新元素
        arrayList.add(newone);

        // 将 ArrayList 转换为新的数组
        String[] newArray = arrayList.toArray(new String[arrayList.size()]);
        return newArray;
    }

    /**
     * 从数组中移除一个值
     *
     * @param arr
     * @param value
     * @return
     */

    public static String[] removeValue(String[] arr, String value) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != value) {
                count++;
            }
        }
        String[] newArray = new String[count];
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != value) {
                newArray[index] = arr[i];
                index++;
            }
        }
        return newArray;
    }


    public static boolean findStr(String[] args,String str){
        boolean result = false;
        //第一种：List
        result = Arrays.asList(args).contains(str);


        return result;
    }

}
