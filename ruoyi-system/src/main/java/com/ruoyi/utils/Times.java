package com.ruoyi.utils;

import com.ruoyi.scheduling.domain.ScheduCount;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Times {
    /**
     * 获取本月的日期
     * @param year
     * @param month
     * @return
     */
    public static List<ScheduCount>  getDays(String year,String month){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
//        int year = 2024;
//        int month = 01;
        calendar.set(Integer.parseInt(year), Integer.parseInt(month) - 1, 1);
        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        List<Date> dateList = new ArrayList<>();
        for (int i = 0; i < daysInMonth; i++) {
            Date date = calendar.getTime();
            dateList.add(date);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        List<ScheduCount> dateList2 = new ArrayList<>();
        for (int i=0; i<dateList.size(); i++) {


            //将传入的时间解析成Date类型,相当于格式化
            String dBegin = sdf.format(dateList.get(i));
            ScheduCount  scheduCount=new ScheduCount();
            scheduCount.setDate(dBegin);
            scheduCount.setContent(0);
            dateList2.add(scheduCount);

        }
        return dateList2;
    }





    /**
     *判断日期是否在此范围内
     */
        public static void main(String[] args) throws Exception{

        }

        public  static String  StateType(String startTimeStr,String endTimeStr) throws ParseException {

            System.out.println(startTimeStr);
            System.out.println(endTimeStr);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); // 设置日期格式

//            String startTimeStr = "2024-01-29 09:30"; // 起始时间
            Date startTime = sdf.parse(startTimeStr);

//            String endTimeStr = "2024-01-29 18:00"; // 结束时间
            Date endTime = sdf.parse(endTimeStr);

            Calendar now = Calendar.getInstance(); // 获取当前时间
            Date currentTime = now.getTime();

            if (currentTime.before(startTime)) {
                //未开始
                System.out.println("当前时间在" + startTimeStr + "之前");
                return "未开始";

            } else if (currentTime.after(endTime)){
                System.out.println("当前时间在" + endTimeStr + "之后");
                //待结算
                return "待结算";
            } else {
                //进行中
                System.out.println("当前时间在" + startTimeStr + "-" + endTimeStr + "之间");
                return "进行中";
            }
        }

}
