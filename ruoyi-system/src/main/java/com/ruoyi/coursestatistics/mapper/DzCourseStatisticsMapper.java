package com.ruoyi.coursestatistics.mapper;

import java.util.List;
import com.ruoyi.coursestatistics.domain.DzCourseStatistics;

/**
 * 课程统计Mapper接口
 * 
 * @author ruoyi
 * @date 2024-02-02
 */
public interface DzCourseStatisticsMapper 
{
    /**
     * 查询课程统计
     * 
     * @param id 课程统计主键
     * @return 课程统计
     */
    public DzCourseStatistics selectDzCourseStatisticsById(Long id);

    /**
     * 查询课程统计列表
     * 
     * @param dzCourseStatistics 课程统计
     * @return 课程统计集合
     */
    public List<DzCourseStatistics> selectDzCourseStatisticsList(DzCourseStatistics dzCourseStatistics);

    /**
     * 新增课程统计
     * 
     * @param dzCourseStatistics 课程统计
     * @return 结果
     */
    public int insertDzCourseStatistics(DzCourseStatistics dzCourseStatistics);

    /**
     * 修改课程统计
     * 
     * @param dzCourseStatistics 课程统计
     * @return 结果
     */
    public int updateDzCourseStatistics(DzCourseStatistics dzCourseStatistics);

    /**
     * 删除课程统计
     * 
     * @param id 课程统计主键
     * @return 结果
     */
    public int deleteDzCourseStatisticsById(Long id);

    /**
     * 批量删除课程统计
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzCourseStatisticsByIds(Long[] ids);
}
