package com.ruoyi.coursestatistics.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 课程统计对象 dz_course_statistics
 * 
 * @author ruoyi
 * @date 2024-02-02
 */
public class DzCourseStatistics extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 机构ID */
    @Excel(name = "机构ID")
    private String deptId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private String userId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** 用户课程 */
    @Excel(name = "用户课程")
    private String userCourse;

    /** 总课时 */
    @Excel(name = "总课时")
    private String userTotalClassHour;

    /** 消耗课时 */
    @Excel(name = "消耗课时")
    private String userConsumeClassHour;

    /** 备注1 */
    @Excel(name = "备注1")
    private String remark1;

    /** 备注2 */
    @Excel(name = "备注2")
    private String remark2;

    /** 备注3 */
    @Excel(name = "备注3")
    private String remark3;

    /** 备注4 */
    @Excel(name = "备注4")
    private String remark4;

    /** 备注5 */
    @Excel(name = "备注5")
    private String remark5;

    /** 备注6 */
    @Excel(name = "备注6")
    private String remark6;

    /** 备注7 */
    @Excel(name = "备注7")
    private String remark7;

    /** 备注8 */
    @Excel(name = "备注8")
    private String remark8;

    /** 备注9 */
    @Excel(name = "备注9")
    private String remark9;

    /** 备注10 */
    @Excel(name = "备注10")
    private String remark10;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDeptId(String deptId) 
    {
        this.deptId = deptId;
    }

    public String getDeptId() 
    {
        return deptId;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setUserCourse(String userCourse) 
    {
        this.userCourse = userCourse;
    }

    public String getUserCourse() 
    {
        return userCourse;
    }
    public void setUserTotalClassHour(String userTotalClassHour) 
    {
        this.userTotalClassHour = userTotalClassHour;
    }

    public String getUserTotalClassHour() 
    {
        return userTotalClassHour;
    }
    public void setUserConsumeClassHour(String userConsumeClassHour) 
    {
        this.userConsumeClassHour = userConsumeClassHour;
    }

    public String getUserConsumeClassHour() 
    {
        return userConsumeClassHour;
    }
    public void setRemark1(String remark1) 
    {
        this.remark1 = remark1;
    }

    public String getRemark1() 
    {
        return remark1;
    }
    public void setRemark2(String remark2) 
    {
        this.remark2 = remark2;
    }

    public String getRemark2() 
    {
        return remark2;
    }
    public void setRemark3(String remark3) 
    {
        this.remark3 = remark3;
    }

    public String getRemark3() 
    {
        return remark3;
    }
    public void setRemark4(String remark4) 
    {
        this.remark4 = remark4;
    }

    public String getRemark4() 
    {
        return remark4;
    }
    public void setRemark5(String remark5) 
    {
        this.remark5 = remark5;
    }

    public String getRemark5() 
    {
        return remark5;
    }
    public void setRemark6(String remark6) 
    {
        this.remark6 = remark6;
    }

    public String getRemark6() 
    {
        return remark6;
    }
    public void setRemark7(String remark7) 
    {
        this.remark7 = remark7;
    }

    public String getRemark7() 
    {
        return remark7;
    }
    public void setRemark8(String remark8) 
    {
        this.remark8 = remark8;
    }

    public String getRemark8() 
    {
        return remark8;
    }
    public void setRemark9(String remark9) 
    {
        this.remark9 = remark9;
    }

    public String getRemark9() 
    {
        return remark9;
    }
    public void setRemark10(String remark10) 
    {
        this.remark10 = remark10;
    }

    public String getRemark10() 
    {
        return remark10;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("deptId", getDeptId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("userCourse", getUserCourse())
            .append("userTotalClassHour", getUserTotalClassHour())
            .append("userConsumeClassHour", getUserConsumeClassHour())
            .append("remark1", getRemark1())
            .append("remark2", getRemark2())
            .append("remark3", getRemark3())
            .append("remark4", getRemark4())
            .append("remark5", getRemark5())
            .append("remark6", getRemark6())
            .append("remark7", getRemark7())
            .append("remark8", getRemark8())
            .append("remark9", getRemark9())
            .append("remark10", getRemark10())
            .toString();
    }
}
