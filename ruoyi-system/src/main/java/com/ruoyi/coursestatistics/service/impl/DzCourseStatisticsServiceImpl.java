package com.ruoyi.coursestatistics.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.coursestatistics.mapper.DzCourseStatisticsMapper;
import com.ruoyi.coursestatistics.domain.DzCourseStatistics;
import com.ruoyi.coursestatistics.service.IDzCourseStatisticsService;

/**
 * 课程统计Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-02-02
 */
@Service
public class DzCourseStatisticsServiceImpl implements IDzCourseStatisticsService 
{
    @Autowired
    private DzCourseStatisticsMapper dzCourseStatisticsMapper;

    /**
     * 查询课程统计
     * 
     * @param id 课程统计主键
     * @return 课程统计
     */
    @Override
    public DzCourseStatistics selectDzCourseStatisticsById(Long id)
    {
        return dzCourseStatisticsMapper.selectDzCourseStatisticsById(id);
    }

    /**
     * 查询课程统计列表
     * 
     * @param dzCourseStatistics 课程统计
     * @return 课程统计
     */
    @Override
    public List<DzCourseStatistics> selectDzCourseStatisticsList(DzCourseStatistics dzCourseStatistics)
    {
        return dzCourseStatisticsMapper.selectDzCourseStatisticsList(dzCourseStatistics);
    }

    /**
     * 新增课程统计
     * 
     * @param dzCourseStatistics 课程统计
     * @return 结果
     */
    @Override
    public int insertDzCourseStatistics(DzCourseStatistics dzCourseStatistics)
    {
        return dzCourseStatisticsMapper.insertDzCourseStatistics(dzCourseStatistics);
    }

    /**
     * 修改课程统计
     * 
     * @param dzCourseStatistics 课程统计
     * @return 结果
     */
    @Override
    public int updateDzCourseStatistics(DzCourseStatistics dzCourseStatistics)
    {
        return dzCourseStatisticsMapper.updateDzCourseStatistics(dzCourseStatistics);
    }

    /**
     * 批量删除课程统计
     * 
     * @param ids 需要删除的课程统计主键
     * @return 结果
     */
    @Override
    public int deleteDzCourseStatisticsByIds(Long[] ids)
    {
        return dzCourseStatisticsMapper.deleteDzCourseStatisticsByIds(ids);
    }

    /**
     * 删除课程统计信息
     * 
     * @param id 课程统计主键
     * @return 结果
     */
    @Override
    public int deleteDzCourseStatisticsById(Long id)
    {
        return dzCourseStatisticsMapper.deleteDzCourseStatisticsById(id);
    }
}
