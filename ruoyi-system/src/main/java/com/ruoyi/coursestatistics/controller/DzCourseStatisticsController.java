package com.ruoyi.coursestatistics.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.coursestatistics.domain.DzCourseStatistics;
import com.ruoyi.coursestatistics.service.IDzCourseStatisticsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程统计Controller
 * 
 * @author ruoyi
 * @date 2024-02-02
 */
@RestController
@RequestMapping("/coursestatistics/coursestatistics")
public class DzCourseStatisticsController extends BaseController
{
    @Autowired
    private IDzCourseStatisticsService dzCourseStatisticsService;

    /**
     * 查询课程统计列表
     */
    @PreAuthorize("@ss.hasPermi('coursestatistics:coursestatistics:list')")
    @GetMapping("/list")
    public TableDataInfo list(DzCourseStatistics dzCourseStatistics)
    {
        startPage();
        List<DzCourseStatistics> list = dzCourseStatisticsService.selectDzCourseStatisticsList(dzCourseStatistics);
        return getDataTable(list);
    }

    /**
     * 导出课程统计列表
     */
    @PreAuthorize("@ss.hasPermi('coursestatistics:coursestatistics:export')")
    @Log(title = "课程统计", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzCourseStatistics dzCourseStatistics)
    {
        List<DzCourseStatistics> list = dzCourseStatisticsService.selectDzCourseStatisticsList(dzCourseStatistics);
        ExcelUtil<DzCourseStatistics> util = new ExcelUtil<DzCourseStatistics>(DzCourseStatistics.class);
        util.exportExcel(response, list, "课程统计数据");
    }

    /**
     * 获取课程统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('coursestatistics:coursestatistics:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dzCourseStatisticsService.selectDzCourseStatisticsById(id));
    }

    /**
     * 新增课程统计
     */
    @PreAuthorize("@ss.hasPermi('coursestatistics:coursestatistics:add')")
    @Log(title = "课程统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzCourseStatistics dzCourseStatistics)
    {
        return toAjax(dzCourseStatisticsService.insertDzCourseStatistics(dzCourseStatistics));
    }

    /**
     * 修改课程统计
     */
    @PreAuthorize("@ss.hasPermi('coursestatistics:coursestatistics:edit')")
    @Log(title = "课程统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DzCourseStatistics dzCourseStatistics)
    {
        return toAjax(dzCourseStatisticsService.updateDzCourseStatistics(dzCourseStatistics));
    }

    /**
     * 删除课程统计
     */
    @PreAuthorize("@ss.hasPermi('coursestatistics:coursestatistics:remove')")
    @Log(title = "课程统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dzCourseStatisticsService.deleteDzCourseStatisticsByIds(ids));
    }
}
