package com.ruoyi.bigdata.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.bigdata.mapper.BigdataMapper;
import com.ruoyi.bigdata.domain.Bigdata;
import com.ruoyi.bigdata.service.IBigdataService;

/**
 * 数据中心Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
@Service
public class BigdataServiceImpl implements IBigdataService 
{
    @Autowired
    private BigdataMapper bigdataMapper;

    /**
     * 查询数据中心
     * 
     * @param id 数据中心主键
     * @return 数据中心
     */
    @Override
    public Bigdata selectBigdataById(Long id)
    {
        return bigdataMapper.selectBigdataById(id);
    }

    /**
     * 查询数据中心列表
     * 
     * @param bigdata 数据中心
     * @return 数据中心
     */
    @Override
    public List<Bigdata> selectBigdataList(Bigdata bigdata)
    {
        return bigdataMapper.selectBigdataList(bigdata);
    }

    /**
     * 新增数据中心
     * 
     * @param bigdata 数据中心
     * @return 结果
     */
    @Override
    public int insertBigdata(Bigdata bigdata)
    {
        bigdata.setCreateTime(DateUtils.getNowDate());
        return bigdataMapper.insertBigdata(bigdata);
    }

    /**
     * 修改数据中心
     * 
     * @param bigdata 数据中心
     * @return 结果
     */
    @Override
    public int updateBigdata(Bigdata bigdata)
    {
        bigdata.setUpdateTime(DateUtils.getNowDate());
        return bigdataMapper.updateBigdata(bigdata);
    }

    /**
     * 批量删除数据中心
     * 
     * @param ids 需要删除的数据中心主键
     * @return 结果
     */
    @Override
    public int deleteBigdataByIds(Long[] ids)
    {
        return bigdataMapper.deleteBigdataByIds(ids);
    }

    /**
     * 删除数据中心信息
     * 
     * @param id 数据中心主键
     * @return 结果
     */
    @Override
    public int deleteBigdataById(Long id)
    {
        return bigdataMapper.deleteBigdataById(id);
    }
}
