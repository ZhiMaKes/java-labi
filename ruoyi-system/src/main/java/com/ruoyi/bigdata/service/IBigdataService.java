package com.ruoyi.bigdata.service;

import java.util.List;
import com.ruoyi.bigdata.domain.Bigdata;

/**
 * 数据中心Service接口
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
public interface IBigdataService 
{
    /**
     * 查询数据中心
     * 
     * @param id 数据中心主键
     * @return 数据中心
     */
    public Bigdata selectBigdataById(Long id);

    /**
     * 查询数据中心列表
     * 
     * @param bigdata 数据中心
     * @return 数据中心集合
     */
    public List<Bigdata> selectBigdataList(Bigdata bigdata);

    /**
     * 新增数据中心
     * 
     * @param bigdata 数据中心
     * @return 结果
     */
    public int insertBigdata(Bigdata bigdata);

    /**
     * 修改数据中心
     * 
     * @param bigdata 数据中心
     * @return 结果
     */
    public int updateBigdata(Bigdata bigdata);

    /**
     * 批量删除数据中心
     * 
     * @param ids 需要删除的数据中心主键集合
     * @return 结果
     */
    public int deleteBigdataByIds(Long[] ids);

    /**
     * 删除数据中心信息
     * 
     * @param id 数据中心主键
     * @return 结果
     */
    public int deleteBigdataById(Long id);
}
