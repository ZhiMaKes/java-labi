package com.ruoyi.bigdata.mapper;

import java.util.List;
import com.ruoyi.bigdata.domain.Bigdata;

/**
 * 数据中心Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
public interface BigdataMapper 
{
    /**
     * 查询数据中心
     * 
     * @param id 数据中心主键
     * @return 数据中心
     */
    public Bigdata selectBigdataById(Long id);

    /**
     * 查询数据中心列表
     * 
     * @param bigdata 数据中心
     * @return 数据中心集合
     */
    public List<Bigdata> selectBigdataList(Bigdata bigdata);

    /**
     * 新增数据中心
     * 
     * @param bigdata 数据中心
     * @return 结果
     */
    public int insertBigdata(Bigdata bigdata);

    /**
     * 修改数据中心
     * 
     * @param bigdata 数据中心
     * @return 结果
     */
    public int updateBigdata(Bigdata bigdata);

    /**
     * 删除数据中心
     * 
     * @param id 数据中心主键
     * @return 结果
     */
    public int deleteBigdataById(Long id);

    /**
     * 批量删除数据中心
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBigdataByIds(Long[] ids);
}
