package com.ruoyi.bigdata.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.bigdata.domain.Bigdata;
import com.ruoyi.bigdata.service.IBigdataService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 数据中心Controller
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
@RestController
@RequestMapping("/bigdata/bigdata")
public class BigdataController extends BaseController
{
    @Autowired
    private IBigdataService bigdataService;

    /**
     * 查询数据中心列表
     */
    @PreAuthorize("@ss.hasPermi('bigdata:bigdata:list')")
    @GetMapping("/list")
    public TableDataInfo list(Bigdata bigdata)
    {
        startPage();
        List<Bigdata> list = bigdataService.selectBigdataList(bigdata);
        return getDataTable(list);
    }

    /**
     * 导出数据中心列表
     */
    @PreAuthorize("@ss.hasPermi('bigdata:bigdata:export')")
    @Log(title = "数据中心", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Bigdata bigdata)
    {
        List<Bigdata> list = bigdataService.selectBigdataList(bigdata);
        ExcelUtil<Bigdata> util = new ExcelUtil<Bigdata>(Bigdata.class);
        util.exportExcel(response, list, "数据中心数据");
    }

    /**
     * 获取数据中心详细信息
     */
    @PreAuthorize("@ss.hasPermi('bigdata:bigdata:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bigdataService.selectBigdataById(id));
    }

    /**
     * 新增数据中心
     */
    @PreAuthorize("@ss.hasPermi('bigdata:bigdata:add')")
    @Log(title = "数据中心", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Bigdata bigdata)
    {
        return toAjax(bigdataService.insertBigdata(bigdata));
    }

    /**
     * 修改数据中心
     */
    @PreAuthorize("@ss.hasPermi('bigdata:bigdata:edit')")
    @Log(title = "数据中心", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Bigdata bigdata)
    {
        return toAjax(bigdataService.updateBigdata(bigdata));
    }

    /**
     * 删除数据中心
     */
    @PreAuthorize("@ss.hasPermi('bigdata:bigdata:remove')")
    @Log(title = "数据中心", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bigdataService.deleteBigdataByIds(ids));
    }
}
