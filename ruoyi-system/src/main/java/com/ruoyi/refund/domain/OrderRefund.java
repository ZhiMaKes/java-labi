package com.ruoyi.refund.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 退费对象 dz_order_refund
 * 
 * @author ruoyi
 * @date 2024-02-26
 */
public class OrderRefund extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 退费ID */
    private Long refundId;

    /** 退费学员 */
    @Excel(name = "退费学员")
    private String refundStudentName;

    /** 退费课程 */
    @Excel(name = "退费课程")
    private String refundCourse;

    /** 退费日期 */
    @Excel(name = "退费日期")
    private String refundTime;

    /** 退课数量 */
    @Excel(name = "退课数量")
    private String refundCourseNumber;

    /** 应退金额 */
    @Excel(name = "应退金额")
    private String refundAmount;

    /** 实退金额 */
    @Excel(name = "实退金额")
    private String refundAmountReality;

    /** 盈亏 */
    @Excel(name = "盈亏")
    private String profitAndLoss;

    /** 机构id */
    @Excel(name = "机构id")
    private String deptId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 订单ID */
    @Excel(name = "订单ID")
    private String orderId;

    /** 备注 */
    @Excel(name = "备注")
    private String orderRemark;

    /** 备注2 */
    @Excel(name = "备注2")
    private String orderRemark2;

    /** 备注3 */
    @Excel(name = "备注3")
    private String orderRemark3;

    /** 备注4 */
    @Excel(name = "备注4")
    private String orderRemark4;

    /** 备注5 */
    @Excel(name = "备注5")
    private String orderRemark5;

    /** 备注6 */
    @Excel(name = "备注6")
    private String orderRemark6;

    /** 备注7 */
    @Excel(name = "备注7")
    private String orderRemark7;

    /** 备注8 */
    @Excel(name = "备注8")
    private String orderRemark8;

    /** 备注9 */
    @Excel(name = "备注9")
    private String orderRemark9;

    /** 备注10 */
    @Excel(name = "备注10")
    private String orderRemark10;

    public void setRefundId(Long refundId) 
    {
        this.refundId = refundId;
    }

    public Long getRefundId() 
    {
        return refundId;
    }
    public void setRefundStudentName(String refundStudentName) 
    {
        this.refundStudentName = refundStudentName;
    }

    public String getRefundStudentName() 
    {
        return refundStudentName;
    }
    public void setRefundCourse(String refundCourse) 
    {
        this.refundCourse = refundCourse;
    }

    public String getRefundCourse() 
    {
        return refundCourse;
    }
    public void setRefundTime(String refundTime) 
    {
        this.refundTime = refundTime;
    }

    public String getRefundTime() 
    {
        return refundTime;
    }
    public void setRefundCourseNumber(String refundCourseNumber) 
    {
        this.refundCourseNumber = refundCourseNumber;
    }

    public String getRefundCourseNumber() 
    {
        return refundCourseNumber;
    }
    public void setRefundAmount(String refundAmount) 
    {
        this.refundAmount = refundAmount;
    }

    public String getRefundAmount() 
    {
        return refundAmount;
    }
    public void setRefundAmountReality(String refundAmountReality) 
    {
        this.refundAmountReality = refundAmountReality;
    }

    public String getRefundAmountReality() 
    {
        return refundAmountReality;
    }
    public void setProfitAndLoss(String profitAndLoss) 
    {
        this.profitAndLoss = profitAndLoss;
    }

    public String getProfitAndLoss() 
    {
        return profitAndLoss;
    }
    public void setDeptId(String deptId) 
    {
        this.deptId = deptId;
    }

    public String getDeptId() 
    {
        return deptId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setOrderId(String orderId) 
    {
        this.orderId = orderId;
    }

    public String getOrderId() 
    {
        return orderId;
    }
    public void setOrderRemark(String orderRemark) 
    {
        this.orderRemark = orderRemark;
    }

    public String getOrderRemark() 
    {
        return orderRemark;
    }
    public void setOrderRemark2(String orderRemark2) 
    {
        this.orderRemark2 = orderRemark2;
    }

    public String getOrderRemark2() 
    {
        return orderRemark2;
    }
    public void setOrderRemark3(String orderRemark3) 
    {
        this.orderRemark3 = orderRemark3;
    }

    public String getOrderRemark3() 
    {
        return orderRemark3;
    }
    public void setOrderRemark4(String orderRemark4) 
    {
        this.orderRemark4 = orderRemark4;
    }

    public String getOrderRemark4() 
    {
        return orderRemark4;
    }
    public void setOrderRemark5(String orderRemark5) 
    {
        this.orderRemark5 = orderRemark5;
    }

    public String getOrderRemark5() 
    {
        return orderRemark5;
    }
    public void setOrderRemark6(String orderRemark6) 
    {
        this.orderRemark6 = orderRemark6;
    }

    public String getOrderRemark6() 
    {
        return orderRemark6;
    }
    public void setOrderRemark7(String orderRemark7) 
    {
        this.orderRemark7 = orderRemark7;
    }

    public String getOrderRemark7() 
    {
        return orderRemark7;
    }
    public void setOrderRemark8(String orderRemark8) 
    {
        this.orderRemark8 = orderRemark8;
    }

    public String getOrderRemark8() 
    {
        return orderRemark8;
    }
    public void setOrderRemark9(String orderRemark9) 
    {
        this.orderRemark9 = orderRemark9;
    }

    public String getOrderRemark9() 
    {
        return orderRemark9;
    }
    public void setOrderRemark10(String orderRemark10) 
    {
        this.orderRemark10 = orderRemark10;
    }

    public String getOrderRemark10() 
    {
        return orderRemark10;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("refundId", getRefundId())
            .append("refundStudentName", getRefundStudentName())
            .append("refundCourse", getRefundCourse())
            .append("refundTime", getRefundTime())
            .append("refundCourseNumber", getRefundCourseNumber())
            .append("refundAmount", getRefundAmount())
            .append("refundAmountReality", getRefundAmountReality())
            .append("profitAndLoss", getProfitAndLoss())
            .append("deptId", getDeptId())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("orderId", getOrderId())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("orderRemark", getOrderRemark())
            .append("orderRemark2", getOrderRemark2())
            .append("orderRemark3", getOrderRemark3())
            .append("orderRemark4", getOrderRemark4())
            .append("orderRemark5", getOrderRemark5())
            .append("orderRemark6", getOrderRemark6())
            .append("orderRemark7", getOrderRemark7())
            .append("orderRemark8", getOrderRemark8())
            .append("orderRemark9", getOrderRemark9())
            .append("orderRemark10", getOrderRemark10())
            .toString();
    }
}
