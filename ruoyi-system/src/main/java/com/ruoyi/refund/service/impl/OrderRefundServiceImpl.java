package com.ruoyi.refund.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.refund.mapper.OrderRefundMapper;
import com.ruoyi.refund.domain.OrderRefund;
import com.ruoyi.refund.service.IOrderRefundService;

/**
 * 退费Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-02-26
 */
@Service
public class OrderRefundServiceImpl implements IOrderRefundService 
{
    @Autowired
    private OrderRefundMapper orderRefundMapper;

    /**
     * 查询退费
     * 
     * @param refundId 退费主键
     * @return 退费
     */
    @Override
    public OrderRefund selectOrderRefundByRefundId(Long refundId)
    {
        return orderRefundMapper.selectOrderRefundByRefundId(refundId);
    }

    /**
     * 查询退费列表
     * 
     * @param orderRefund 退费
     * @return 退费
     */
    @Override
    public List<OrderRefund> selectOrderRefundList(OrderRefund orderRefund)
    {
        return orderRefundMapper.selectOrderRefundList(orderRefund);
    }

    /**
     * 新增退费
     * 
     * @param orderRefund 退费
     * @return 结果
     */
    @Override
    public int insertOrderRefund(OrderRefund orderRefund)
    {
        orderRefund.setCreateTime(DateUtils.getNowDate());
        return orderRefundMapper.insertOrderRefund(orderRefund);
    }

    /**
     * 修改退费
     * 
     * @param orderRefund 退费
     * @return 结果
     */
    @Override
    public int updateOrderRefund(OrderRefund orderRefund)
    {
        orderRefund.setUpdateTime(DateUtils.getNowDate());
        return orderRefundMapper.updateOrderRefund(orderRefund);
    }

    /**
     * 批量删除退费
     * 
     * @param refundIds 需要删除的退费主键
     * @return 结果
     */
    @Override
    public int deleteOrderRefundByRefundIds(Long[] refundIds)
    {
        return orderRefundMapper.deleteOrderRefundByRefundIds(refundIds);
    }

    /**
     * 删除退费信息
     * 
     * @param refundId 退费主键
     * @return 结果
     */
    @Override
    public int deleteOrderRefundByRefundId(Long refundId)
    {
        return orderRefundMapper.deleteOrderRefundByRefundId(refundId);
    }
}
