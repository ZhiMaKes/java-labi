package com.ruoyi.refund.service;

import java.util.List;
import com.ruoyi.refund.domain.OrderRefund;

/**
 * 退费Service接口
 * 
 * @author ruoyi
 * @date 2024-02-26
 */
public interface IOrderRefundService 
{
    /**
     * 查询退费
     * 
     * @param refundId 退费主键
     * @return 退费
     */
    public OrderRefund selectOrderRefundByRefundId(Long refundId);

    /**
     * 查询退费列表
     * 
     * @param orderRefund 退费
     * @return 退费集合
     */
    public List<OrderRefund> selectOrderRefundList(OrderRefund orderRefund);

    /**
     * 新增退费
     * 
     * @param orderRefund 退费
     * @return 结果
     */
    public int insertOrderRefund(OrderRefund orderRefund);

    /**
     * 修改退费
     * 
     * @param orderRefund 退费
     * @return 结果
     */
    public int updateOrderRefund(OrderRefund orderRefund);

    /**
     * 批量删除退费
     * 
     * @param refundIds 需要删除的退费主键集合
     * @return 结果
     */
    public int deleteOrderRefundByRefundIds(Long[] refundIds);

    /**
     * 删除退费信息
     * 
     * @param refundId 退费主键
     * @return 结果
     */
    public int deleteOrderRefundByRefundId(Long refundId);
}
