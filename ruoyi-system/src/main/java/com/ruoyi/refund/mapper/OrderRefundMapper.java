package com.ruoyi.refund.mapper;

import java.util.List;
import com.ruoyi.refund.domain.OrderRefund;

/**
 * 退费Mapper接口
 * 
 * @author ruoyi
 * @date 2024-02-26
 */
public interface OrderRefundMapper 
{
    /**
     * 查询退费
     * 
     * @param refundId 退费主键
     * @return 退费
     */
    public OrderRefund selectOrderRefundByRefundId(Long refundId);

    /**
     * 查询退费列表
     * 
     * @param orderRefund 退费
     * @return 退费集合
     */
    public List<OrderRefund> selectOrderRefundList(OrderRefund orderRefund);

    /**
     * 新增退费
     * 
     * @param orderRefund 退费
     * @return 结果
     */
    public int insertOrderRefund(OrderRefund orderRefund);

    /**
     * 修改退费
     * 
     * @param orderRefund 退费
     * @return 结果
     */
    public int updateOrderRefund(OrderRefund orderRefund);

    /**
     * 删除退费
     * 
     * @param refundId 退费主键
     * @return 结果
     */
    public int deleteOrderRefundByRefundId(Long refundId);

    /**
     * 批量删除退费
     * 
     * @param refundIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderRefundByRefundIds(Long[] refundIds);
}
