package com.ruoyi.scheduling.mapper;

import java.util.List;

import com.ruoyi.scheduling.domain.DzCourseScheduling;
import com.ruoyi.scheduling.domain.ScheduCount;
import org.apache.ibatis.annotations.Param;

/**
 * 课程表Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface DzCourseSchedulingMapper {
    /**
     * 查询课程表
     *
     * @param id 课程表主键
     * @return 课程表
     */
    public DzCourseScheduling selectDzCourseSchedulingById(Long id);

    public int UPDATEDzCourseSchedulingDayS( );


    public List<ScheduCount> getSchedulingCount(@Param("year") String year,
                                                @Param("month") String month,
                                                @Param("deptId") String deptId,
                                                @Param("classCourse") String classCourse,
                                                @Param("className") String className,
                                                @Param("classTeacher") String classTeacher,
                                                @Param("classStudent") String classStudent);
    public List<ScheduCount> getSchedulingCountUnsettled(@Param("year") String year,
                                                @Param("month") String month,
                                                @Param("deptId") String deptId,
                                                @Param("classCourse") String classCourse,
                                                @Param("className") String className,
                                                @Param("classTeacher") String classTeacher,
                                                @Param("classStudent") String classStudent);


    /**
     * 查询课程表列表
     *
     * @param dzCourseScheduling 课程表
     * @return 课程表集合
     */
    public List<DzCourseScheduling> selectDzCourseSchedulingList(DzCourseScheduling dzCourseScheduling);

    /**
     * 新增课程表
     *
     * @param dzCourseScheduling 课程表
     * @return 结果
     */
    public int insertDzCourseScheduling(DzCourseScheduling dzCourseScheduling);

    /**
     * 修改课程表
     *
     * @param dzCourseScheduling 课程表
     * @return 结果
     */
    public int updateDzCourseScheduling(DzCourseScheduling dzCourseScheduling);

    /**
     * 删除课程表
     *
     * @param id 课程表主键
     * @return 结果
     */
    public int deleteDzCourseSchedulingById(Long id);

    /**
     * 批量删除课程表
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzCourseSchedulingByIds(Long[] ids);

    List<DzCourseScheduling> getDzCourseSchedulingByIds(String[] ids);
}
