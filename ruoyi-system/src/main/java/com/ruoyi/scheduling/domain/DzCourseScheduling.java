package com.ruoyi.scheduling.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 排课对象 dz_course_scheduling
 *
 * @author ruoyi
 * @date 2024-02-05
 */
public class DzCourseScheduling extends BaseEntity
{

    private static final long serialVersionUID = 1L;

    String year;
    String month;
    private String data1;

    private String data2;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getData1() {
        return data1;
    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public String getData2() {
        return data2;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public String getData3() {
        return data3;
    }

    public void setData3(String data3) {
        this.data3 = data3;
    }

    //批量延后天数
    private String data3;

    /** id */
    private Long id;

    /** 机构ID */
    @Excel(name = "机构ID")
    private String deptId;

    /** 班级ID */
    @Excel(name = "班级ID")
    private String classId;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 课程ID */
    @Excel(name = "课程ID")
    private String classCourseId;

    /** 关联课程 */
    @Excel(name = "关联课程")
    private String classCourse;

    /** 班级容量 */
    @Excel(name = "班级容量")
    private String classCapacity;

    /** 请假默认计课时 */
    @Excel(name = "请假默认计课时")
    private String classAskforleave;

    /** 未到默认计课时 */
    @Excel(name = "未到默认计课时")
    private String classNoyet;

    /** 周几排课 */
    @Excel(name = "周几排课")
    private String classWorkOut;

    /** 循环方式 */
    @Excel(name = "循环方式")
    private String classCirculate;

    /** 上课时间开始 */
    @Excel(name = "上课时间开始")
    private String classStart;

    /** 上课时间结束 */
    @Excel(name = "上课时间结束")
    private String classFinish;

    /** 课时数 */
    @Excel(name = "课时数")
    private String classHours;

    /** 授课老师 */
    @Excel(name = "授课老师")
    private String classTeacher;

    /** 考勤员 */
    @Excel(name = "考勤员")
    private String classAttendant;

    /** 上课教室 */
    @Excel(name = "上课教室")
    private String classRoom;

    /** 学员列表 */
    @Excel(name = "学员人数")
    private String classStudent;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 备注1 */
    @Excel(name = "备注1")
    private String schedulingremark;

    /** 备注2 */
    @Excel(name = "备注2")
    private String schedulingremark2;

    /** 备注3 */
    @Excel(name = "备注3")
    private String schedulingremark3;

    /** 备注4 */
    @Excel(name = "备注4")
    private String schedulingremark4;

    /** 备注5 */
    @Excel(name = "备注5")
    private String schedulingremark5;

    /** 备注6 */
    @Excel(name = "备注6")
    private String schedulingremark6;

    /** 备注7 */
    @Excel(name = "备注7")
    private String schedulingremark7;

    /** 备注8 */
    @Excel(name = "备注8")
    private String schedulingremark8;

    /** 备注9 */
    @Excel(name = "备注9")
    private String schedulingremark9;

    /** 备注10 */
    @Excel(name = "备注10")
    private String schedulingremark10;

    /** 上课日期 */
    @Excel(name = "上课日期")
    private String studyDate;

    /** 星期几 */
    @Excel(name = "星期几")
    private String week;

    /** 状态(未开始,进行中,待结算,已结算) */
    @Excel(name = "状态(未开始,进行中,待结算,已结算)")
    private String state;

    /** 到达人数 */
    @Excel(name = "到达人数")
    private String reach;

    /** 未到达人数 */
    @Excel(name = "未到达人数")
    private String unreach;

    /** 请假人数 */
    @Excel(name = "请假人数")
    private String askforleave;

    /** 出勤率 */
    @Excel(name = "出勤率")
    private String attendance;

    /** 累计消耗时长 */
    @Excel(name = "累计消耗时长")
    private String totalConsumption;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setDeptId(String deptId)
    {
        this.deptId = deptId;
    }

    public String getDeptId()
    {
        return deptId;
    }
    public void setClassId(String classId)
    {
        this.classId = classId;
    }

    public String getClassId()
    {
        return classId;
    }
    public void setClassName(String className)
    {
        this.className = className;
    }

    public String getClassName()
    {
        return className;
    }
    public void setClassCourseId(String classCourseId)
    {
        this.classCourseId = classCourseId;
    }

    public String getClassCourseId()
    {
        return classCourseId;
    }
    public void setClassCourse(String classCourse)
    {
        this.classCourse = classCourse;
    }

    public String getClassCourse()
    {
        return classCourse;
    }
    public void setClassCapacity(String classCapacity)
    {
        this.classCapacity = classCapacity;
    }

    public String getClassCapacity()
    {
        return classCapacity;
    }
    public void setClassAskforleave(String classAskforleave)
    {
        this.classAskforleave = classAskforleave;
    }

    public String getClassAskforleave()
    {
        return classAskforleave;
    }
    public void setClassNoyet(String classNoyet)
    {
        this.classNoyet = classNoyet;
    }

    public String getClassNoyet()
    {
        return classNoyet;
    }
    public void setClassWorkOut(String classWorkOut)
    {
        this.classWorkOut = classWorkOut;
    }

    public String getClassWorkOut()
    {
        return classWorkOut;
    }
    public void setClassCirculate(String classCirculate)
    {
        this.classCirculate = classCirculate;
    }

    public String getClassCirculate()
    {
        return classCirculate;
    }
    public void setClassStart(String classStart)
    {
        this.classStart = classStart;
    }

    public String getClassStart()
    {
        return classStart;
    }
    public void setClassFinish(String classFinish)
    {
        this.classFinish = classFinish;
    }

    public String getClassFinish()
    {
        return classFinish;
    }
    public void setClassHours(String classHours)
    {
        this.classHours = classHours;
    }

    public String getClassHours()
    {
        return classHours;
    }
    public void setClassTeacher(String classTeacher)
    {
        this.classTeacher = classTeacher;
    }

    public String getClassTeacher()
    {
        return classTeacher;
    }
    public void setClassAttendant(String classAttendant)
    {
        this.classAttendant = classAttendant;
    }

    public String getClassAttendant()
    {
        return classAttendant;
    }
    public void setClassRoom(String classRoom)
    {
        this.classRoom = classRoom;
    }

    public String getClassRoom()
    {
        return classRoom;
    }
    public void setClassStudent(String classStudent)
    {
        this.classStudent = classStudent;
    }

    public String getClassStudent()
    {
        return classStudent;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setSchedulingremark(String schedulingremark)
    {
        this.schedulingremark = schedulingremark;
    }

    public String getSchedulingremark()
    {
        return schedulingremark;
    }
    public void setSchedulingremark2(String schedulingremark2)
    {
        this.schedulingremark2 = schedulingremark2;
    }

    public String getSchedulingremark2()
    {
        return schedulingremark2;
    }
    public void setSchedulingremark3(String schedulingremark3)
    {
        this.schedulingremark3 = schedulingremark3;
    }

    public String getSchedulingremark3()
    {
        return schedulingremark3;
    }
    public void setSchedulingremark4(String schedulingremark4)
    {
        this.schedulingremark4 = schedulingremark4;
    }

    public String getSchedulingremark4()
    {
        return schedulingremark4;
    }
    public void setSchedulingremark5(String schedulingremark5)
    {
        this.schedulingremark5 = schedulingremark5;
    }

    public String getSchedulingremark5()
    {
        return schedulingremark5;
    }
    public void setSchedulingremark6(String schedulingremark6)
    {
        this.schedulingremark6 = schedulingremark6;
    }

    public String getSchedulingremark6()
    {
        return schedulingremark6;
    }
    public void setSchedulingremark7(String schedulingremark7)
    {
        this.schedulingremark7 = schedulingremark7;
    }

    public String getSchedulingremark7()
    {
        return schedulingremark7;
    }
    public void setSchedulingremark8(String schedulingremark8)
    {
        this.schedulingremark8 = schedulingremark8;
    }

    public String getSchedulingremark8()
    {
        return schedulingremark8;
    }
    public void setSchedulingremark9(String schedulingremark9)
    {
        this.schedulingremark9 = schedulingremark9;
    }

    public String getSchedulingremark9()
    {
        return schedulingremark9;
    }
    public void setSchedulingremark10(String schedulingremark10)
    {
        this.schedulingremark10 = schedulingremark10;
    }

    public String getSchedulingremark10()
    {
        return schedulingremark10;
    }
    public void setStudyDate(String studyDate)
    {
        this.studyDate = studyDate;
    }

    public String getStudyDate()
    {
        return studyDate;
    }
    public void setWeek(String week)
    {
        this.week = week;
    }

    public String getWeek()
    {
        return week;
    }
    public void setState(String state)
    {
        this.state = state;
    }

    public String getState()
    {
        return state;
    }
    public void setReach(String reach)
    {
        this.reach = reach;
    }

    public String getReach()
    {
        return reach;
    }
    public void setUnreach(String unreach)
    {
        this.unreach = unreach;
    }

    public String getUnreach()
    {
        return unreach;
    }
    public void setAskforleave(String askforleave)
    {
        this.askforleave = askforleave;
    }

    public String getAskforleave()
    {
        return askforleave;
    }
    public void setAttendance(String attendance)
    {
        this.attendance = attendance;
    }

    public String getAttendance()
    {
        return attendance;
    }
    public void setTotalConsumption(String totalConsumption)
    {
        this.totalConsumption = totalConsumption;
    }

    public String getTotalConsumption()
    {
        return totalConsumption;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("deptId", getDeptId())
                .append("classId", getClassId())
                .append("className", getClassName())
                .append("classCourseId", getClassCourseId())
                .append("classCourse", getClassCourse())
                .append("classCapacity", getClassCapacity())
                .append("classAskforleave", getClassAskforleave())
                .append("classNoyet", getClassNoyet())
                .append("classWorkOut", getClassWorkOut())
                .append("classCirculate", getClassCirculate())
                .append("classStart", getClassStart())
                .append("classFinish", getClassFinish())
                .append("classHours", getClassHours())
                .append("classTeacher", getClassTeacher())
                .append("classAttendant", getClassAttendant())
                .append("classRoom", getClassRoom())
                .append("classStudent", getClassStudent())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("schedulingremark", getSchedulingremark())
                .append("schedulingremark2", getSchedulingremark2())
                .append("schedulingremark3", getSchedulingremark3())
                .append("schedulingremark4", getSchedulingremark4())
                .append("schedulingremark5", getSchedulingremark5())
                .append("schedulingremark6", getSchedulingremark6())
                .append("schedulingremark7", getSchedulingremark7())
                .append("schedulingremark8", getSchedulingremark8())
                .append("schedulingremark9", getSchedulingremark9())
                .append("schedulingremark10", getSchedulingremark10())
                .append("studyDate", getStudyDate())
                .append("week", getWeek())
                .append("state", getState())
                .append("reach", getReach())
                .append("unreach", getUnreach())
                .append("askforleave", getAskforleave())
                .append("attendance", getAttendance())
                .append("totalConsumption", getTotalConsumption())
                .toString();
    }
}
