package com.ruoyi.scheduling.service;

import java.util.List;
import com.ruoyi.scheduling.domain.DzCourseScheduling;
import com.ruoyi.scheduling.domain.ScheduCount;
import org.apache.ibatis.annotations.Param;

/**
 * 课程表Service接口
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public  interface IDzCourseSchedulingService
{
    /**
     * 查询课程表
     * 
     * @param id 课程表主键
     * @return 课程表
     */
    public DzCourseScheduling selectDzCourseSchedulingById(Long id);

    public int UPDATEDzCourseSchedulingDayS( );

    public List<ScheduCount> getSchedulingCount( String year, String month,   String deptId,
                                                 String classCourse,
                                                 String className,
                                                 String classTeacher,
                                                 String classStudent);
    public List<ScheduCount> getSchedulingCountUnsettled( String year, String month,   String deptId,
                                                 String classCourse,
                                                 String className,
                                                 String classTeacher,
                                                 String classStudent);


    /**
     * 查询课程表列表
     * 
     * @param dzCourseScheduling 课程表
     * @return 课程表集合
     */
    public List<DzCourseScheduling> selectDzCourseSchedulingList(DzCourseScheduling dzCourseScheduling);

    /**
     * 新增课程表
     * 
     * @param dzCourseScheduling 课程表
     * @return 结果
     */
    public int insertDzCourseScheduling(DzCourseScheduling dzCourseScheduling);

    /**
     * 修改课程表
     * 
     * @param dzCourseScheduling 课程表
     * @return 结果
     */
    public int updateDzCourseScheduling(DzCourseScheduling dzCourseScheduling);

    /**
     * 批量删除课程表
     * 
     * @param ids 需要删除的课程表主键集合
     * @return 结果
     */
    public int deleteDzCourseSchedulingByIds(Long[] ids);

    List<DzCourseScheduling> getDzCourseSchedulingByIds(String[] ids);
    /**
     * 删除课程表信息
     * 
     * @param id 课程表主键
     * @return 结果
     */
    public int deleteDzCourseSchedulingById(Long id);
}
