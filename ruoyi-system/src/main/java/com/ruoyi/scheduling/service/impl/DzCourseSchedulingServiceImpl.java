package com.ruoyi.scheduling.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.scheduling.domain.ScheduCount;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.scheduling.mapper.DzCourseSchedulingMapper;
import com.ruoyi.scheduling.domain.DzCourseScheduling;
import com.ruoyi.scheduling.service.IDzCourseSchedulingService;

/**
 * 课程表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
@Service
public class DzCourseSchedulingServiceImpl implements IDzCourseSchedulingService
{
    @Autowired
    private DzCourseSchedulingMapper dzCourseSchedulingMapper;

    /**
     * 查询课程表
     * 
     * @param id 课程表主键
     * @return 课程表
     */
    @Override
    public DzCourseScheduling selectDzCourseSchedulingById(Long id)
    {
        return dzCourseSchedulingMapper.selectDzCourseSchedulingById(id);
    }

    @Override
    public int UPDATEDzCourseSchedulingDayS()
    {
        return dzCourseSchedulingMapper.UPDATEDzCourseSchedulingDayS();
    }


    @Override
    public List<ScheduCount> getSchedulingCount(  String year, String month,   String deptId,
                                                  String classCourse,
                                                    String className,
                                                  String classTeacher,
                                                  String classStudent)
    {
        return dzCourseSchedulingMapper.getSchedulingCount(year,month,deptId,   classCourse,
                  className,
                  classTeacher,
                  classStudent);
    }
    @Override
    public List<ScheduCount> getSchedulingCountUnsettled(  String year, String month,   String deptId,
                                                  String classCourse,
                                                  String className,
                                                  String classTeacher,
                                                  String classStudent)
    {
        return dzCourseSchedulingMapper.getSchedulingCountUnsettled(year,month,deptId,   classCourse,
                className,
                classTeacher,
                classStudent);
    }



    /**
     * 查询课程表列表
     * 
     * @param dzCourseScheduling 课程表
     * @return 课程表
     */
    @Override
    public List<DzCourseScheduling> selectDzCourseSchedulingList(DzCourseScheduling dzCourseScheduling)
    {
        return dzCourseSchedulingMapper.selectDzCourseSchedulingList(dzCourseScheduling);
    }

    /**
     * 新增课程表
     * 
     * @param dzCourseScheduling 课程表
     * @return 结果
     */
    @Override
    public int insertDzCourseScheduling(DzCourseScheduling dzCourseScheduling)
    {
        dzCourseScheduling.setCreateTime(DateUtils.getNowDate());
        return dzCourseSchedulingMapper.insertDzCourseScheduling(dzCourseScheduling);
    }

    /**
     * 修改课程表
     * 
     * @param dzCourseScheduling 课程表
     * @return 结果
     */
    @Override
    public int updateDzCourseScheduling(DzCourseScheduling dzCourseScheduling)
    {
        dzCourseScheduling.setUpdateTime(DateUtils.getNowDate());
        return dzCourseSchedulingMapper.updateDzCourseScheduling(dzCourseScheduling);
    }

    /**
     * 批量删除课程表
     * 
     * @param ids 需要删除的课程表主键
     * @return 结果
     */
    @Override
    public int deleteDzCourseSchedulingByIds(Long[] ids)
    {
        return dzCourseSchedulingMapper.deleteDzCourseSchedulingByIds(ids);
    }


    @Override
    public  List<DzCourseScheduling> getDzCourseSchedulingByIds(String[] ids)
    {
        return dzCourseSchedulingMapper.getDzCourseSchedulingByIds(ids);
    }

    /**
     * 删除课程表信息
     * 
     * @param id 课程表主键
     * @return 结果
     */
    @Override
    public int deleteDzCourseSchedulingById(Long id)
    {
        return dzCourseSchedulingMapper.deleteDzCourseSchedulingById(id);
    }
}
