package com.ruoyi.order.service;

import com.ruoyi.order.domain.Order;

import java.util.List;

/**
 * 订单
Service接口
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IOrderService 
{
    /**
     * 查询订单

     * 
     * @param orderId 订单
主键
     * @return 订单

     */
    public Order selectOrderByOrderId(Long orderId);

    /**
     * 查询订单
列表
     * 
     * @param order 订单

     * @return 订单
集合
     */
    public List<Order> selectOrderList(Order order);

    public List<Order> selectOrderListByCourse(Order order);


    /**
     * 新增订单

     * 
     * @param order 订单

     * @return 结果
     */
    public int insertOrder(Order order);

    /**
     * 修改订单

     * 
     * @param order 订单

     * @return 结果
     */
    public int updateOrder(Order order);

    /**
     * 批量删除订单

     * 
     * @param orderIds 需要删除的订单
主键集合
     * @return 结果
     */
    public int deleteOrderByOrderIds(Long[] orderIds);

    /**
     * 删除订单
信息
     * 
     * @param orderId 订单
主键
     * @return 结果
     */
    public int deleteOrderByOrderId(Long orderId);
}
