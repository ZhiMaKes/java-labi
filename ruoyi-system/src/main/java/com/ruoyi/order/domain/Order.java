package com.ruoyi.order.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单对象 dz_order
 *
 * @author ruoyi
 * @date 2024-02-02
 */
public class Order extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long orderId;

    @Excel(name = "合同编号")
    private String orderRemark2;

    /** 报名学员 */
    @Excel(name = "学员姓名")
    private String orderStudentName;
    /** 经办日期 */
    @Excel(name = "经办日期")
    private String orderTime;

    /** 报名课程 */
    @Excel(name = "课程名称")
    private String orderCourse;



    /** 购课类型 */
    @Excel(name = "课时类型")
    private String orderType;

    /** 购买课时数 */
    @Excel(name = "购买课时数")
    private String orderCourseNumber;


    /** 赠送课时数 */
    @Excel(name = "赠送课时数")
    private String orderFreecourseNumber;
    /** 课时总数 */
    @Excel(name = "课时总数")
    private String totalClassHours;



    /** 应收金额 */
    @Excel(name = "应收金额")
    private String amountReceivable;

    /** 优惠金额 */
    @Excel(name = "优惠金额")
    private String discountAmount;

    /** 实收金额 */
    @Excel(name = "购课金额")
    private String actualAmountReceived;
    /** 单价 */
    @Excel(name = "单价")
    private String unitPrice;
    /** 有效期 */
//    @Excel(name = "有效期")
    @Excel(name = "有效期", readConverterExp = "0=长期")
    private String orderCourseEndtime;

    /** 其他费用总额 */
    @Excel(name = "其他费用总额")
    private String moneyOtherTotal;

    /** 订单总额(课时费+其他费用) */
    @Excel(name = "订单总额(课时费+其他费用)")
    private String moneyOrderTotal;

    /** 退课数量 */
    @Excel(name = "退课数量")
    private String droppedClassesNumber;

    /** 退款总额 */
    @Excel(name = "退款总额")
    private String moneyDroppedTotal;

    /** 盈亏 */
    @Excel(name = "盈亏")
    private String profitAndLoss;



    /** 已消耗课时 */
    @Excel(name = "已消耗课时数")
    private String consumed;


    /** 剩余课时 */
    @Excel(name = "剩余课时")
    private String residue;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 备注 */
//    @Excel(name = "备注")
    private String orderRemark;




    /** 备注3 */
//    @Excel(name = "备注3")
    private String orderRemark3;

    /** 备注4 */
//    @Excel(name = "备注4")
    private String orderRemark4;

    /** 备注5 */
//    @Excel(name = "备注5")
    private String orderRemark5;

    /** 备注6 */
//    @Excel(name = "备注6")
    private String orderRemark6;

    /** 备注7 */
//    @Excel(name = "备注7")
    private String orderRemark7;

    /** 备注8 */
//    @Excel(name = "备注8")
    private String orderRemark8;

    /** 备注9 */
//    @Excel(name = "备注9")
    private String orderRemark9;

    /** 备注10 */
//    @Excel(name = "备注10")
    private String orderRemark10;

    /** 已确认收入 */
//    @Excel(name = "已确认收入")
    private String moneyDetermined;

    /** 未确认收入 */
//    @Excel(name = "未确认收入")
    private String moneyUnconfirmed;



    /** 其他费用JSON */
//    @Excel(name = "其他费用JSON")
    private String otherJson;





    /** 单位 */
//    @Excel(name = "单位")
    private String unit;

    /** 状态 */
//    @Excel(name = "状态")
    private String state;

    /** 销售人员 */
//    @Excel(name = "销售人员")
    private String saleName;

    /** 机构id */
//    @Excel(name = "机构id")
    private String deptId;

    /** 学员id */
//    @Excel(name = "学员id")
    private String studentId;

    /** 业绩分配列表ID */
//    @Excel(name = "业绩分配列表ID")
    private String saleId;

    /** 销售JSON */
//    @Excel(name = "销售JSON")
    private String saleJson;

    public void setOrderId(Long orderId)
    {
        this.orderId = orderId;
    }

    public Long getOrderId()
    {
        return orderId;
    }
    public void setOrderStudentName(String orderStudentName)
    {
        this.orderStudentName = orderStudentName;
    }

    public String getOrderStudentName()
    {
        return orderStudentName;
    }
    public void setOrderCourse(String orderCourse)
    {
        this.orderCourse = orderCourse;
    }

    public String getOrderCourse()
    {
        return orderCourse;
    }
    public void setOrderTime(String orderTime)
    {
        this.orderTime = orderTime;
    }

    public String getOrderTime()
    {
        return orderTime;
    }
    public void setOrderType(String orderType)
    {
        this.orderType = orderType;
    }

    public String getOrderType()
    {
        return orderType;
    }
    public void setOrderCourseNumber(String orderCourseNumber)
    {
        this.orderCourseNumber = orderCourseNumber;
    }

    public String getOrderCourseNumber()
    {
        return orderCourseNumber;
    }
    public void setOrderFreecourseNumber(String orderFreecourseNumber)
    {
        this.orderFreecourseNumber = orderFreecourseNumber;
    }

    public String getOrderFreecourseNumber()
    {
        return orderFreecourseNumber;
    }
    public void setOrderCourseEndtime(String orderCourseEndtime)
    {
        this.orderCourseEndtime = orderCourseEndtime;
    }

    public String getOrderCourseEndtime()
    {
        return orderCourseEndtime;
    }
    public void setAmountReceivable(String amountReceivable)
    {
        this.amountReceivable = amountReceivable;
    }

    public String getAmountReceivable()
    {
        return amountReceivable;
    }
    public void setDiscountAmount(String discountAmount)
    {
        this.discountAmount = discountAmount;
    }

    public String getDiscountAmount()
    {
        return discountAmount;
    }
    public void setActualAmountReceived(String actualAmountReceived)
    {
        this.actualAmountReceived = actualAmountReceived;
    }

    public String getActualAmountReceived()
    {
        return actualAmountReceived;
    }
    public void setTotalClassHours(String totalClassHours)
    {
        this.totalClassHours = totalClassHours;
    }

    public String getTotalClassHours()
    {
        return totalClassHours;
    }
    public void setConsumed(String consumed)
    {
        this.consumed = consumed;
    }

    public String getConsumed()
    {
        return consumed;
    }
    public void setResidue(String residue)
    {
        this.residue = residue;
    }

    public String getResidue()
    {
        return residue;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setOrderRemark(String orderRemark)
    {
        this.orderRemark = orderRemark;
    }

    public String getOrderRemark()
    {
        return orderRemark;
    }
    public void setOrderRemark2(String orderRemark2)
    {
        this.orderRemark2 = orderRemark2;
    }

    public String getOrderRemark2()
    {
        return orderRemark2;
    }
    public void setOrderRemark3(String orderRemark3)
    {
        this.orderRemark3 = orderRemark3;
    }

    public String getOrderRemark3()
    {
        return orderRemark3;
    }
    public void setOrderRemark4(String orderRemark4)
    {
        this.orderRemark4 = orderRemark4;
    }

    public String getOrderRemark4()
    {
        return orderRemark4;
    }
    public void setOrderRemark5(String orderRemark5)
    {
        this.orderRemark5 = orderRemark5;
    }

    public String getOrderRemark5()
    {
        return orderRemark5;
    }
    public void setOrderRemark6(String orderRemark6)
    {
        this.orderRemark6 = orderRemark6;
    }

    public String getOrderRemark6()
    {
        return orderRemark6;
    }
    public void setOrderRemark7(String orderRemark7)
    {
        this.orderRemark7 = orderRemark7;
    }

    public String getOrderRemark7()
    {
        return orderRemark7;
    }
    public void setOrderRemark8(String orderRemark8)
    {
        this.orderRemark8 = orderRemark8;
    }

    public String getOrderRemark8()
    {
        return orderRemark8;
    }
    public void setOrderRemark9(String orderRemark9)
    {
        this.orderRemark9 = orderRemark9;
    }

    public String getOrderRemark9()
    {
        return orderRemark9;
    }
    public void setOrderRemark10(String orderRemark10)
    {
        this.orderRemark10 = orderRemark10;
    }

    public String getOrderRemark10()
    {
        return orderRemark10;
    }
    public void setMoneyDetermined(String moneyDetermined)
    {
        this.moneyDetermined = moneyDetermined;
    }

    public String getMoneyDetermined()
    {
        return moneyDetermined;
    }
    public void setMoneyUnconfirmed(String moneyUnconfirmed)
    {
        this.moneyUnconfirmed = moneyUnconfirmed;
    }

    public String getMoneyUnconfirmed()
    {
        return moneyUnconfirmed;
    }
    public void setDroppedClassesNumber(String droppedClassesNumber)
    {
        this.droppedClassesNumber = droppedClassesNumber;
    }

    public String getDroppedClassesNumber()
    {
        return droppedClassesNumber;
    }
    public void setMoneyDroppedTotal(String moneyDroppedTotal)
    {
        this.moneyDroppedTotal = moneyDroppedTotal;
    }

    public String getMoneyDroppedTotal()
    {
        return moneyDroppedTotal;
    }
    public void setOtherJson(String otherJson)
    {
        this.otherJson = otherJson;
    }

    public String getOtherJson()
    {
        return otherJson;
    }
    public void setMoneyOtherTotal(String moneyOtherTotal)
    {
        this.moneyOtherTotal = moneyOtherTotal;
    }

    public String getMoneyOtherTotal()
    {
        return moneyOtherTotal;
    }
    public void setMoneyOrderTotal(String moneyOrderTotal)
    {
        this.moneyOrderTotal = moneyOrderTotal;
    }

    public String getMoneyOrderTotal()
    {
        return moneyOrderTotal;
    }
    public void setProfitAndLoss(String profitAndLoss)
    {
        this.profitAndLoss = profitAndLoss;
    }

    public String getProfitAndLoss()
    {
        return profitAndLoss;
    }
    public void setUnitPrice(String unitPrice)
    {
        this.unitPrice = unitPrice;
    }

    public String getUnitPrice()
    {
        return unitPrice;
    }
    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public String getUnit()
    {
        return unit;
    }
    public void setState(String state)
    {
        this.state = state;
    }

    public String getState()
    {
        return state;
    }
    public void setSaleName(String saleName)
    {
        this.saleName = saleName;
    }

    public String getSaleName()
    {
        return saleName;
    }
    public void setDeptId(String deptId)
    {
        this.deptId = deptId;
    }

    public String getDeptId()
    {
        return deptId;
    }
    public void setStudentId(String studentId)
    {
        this.studentId = studentId;
    }

    public String getStudentId()
    {
        return studentId;
    }
    public void setSaleId(String saleId)
    {
        this.saleId = saleId;
    }

    public String getSaleId()
    {
        return saleId;
    }
    public void setSaleJson(String saleJson)
    {
        this.saleJson = saleJson;
    }

    public String getSaleJson()
    {
        return saleJson;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("orderId", getOrderId())
                .append("orderStudentName", getOrderStudentName())
                .append("orderCourse", getOrderCourse())
                .append("orderTime", getOrderTime())
                .append("orderType", getOrderType())
                .append("orderCourseNumber", getOrderCourseNumber())
                .append("orderFreecourseNumber", getOrderFreecourseNumber())
                .append("orderCourseEndtime", getOrderCourseEndtime())
                .append("amountReceivable", getAmountReceivable())
                .append("discountAmount", getDiscountAmount())
                .append("actualAmountReceived", getActualAmountReceived())
                .append("totalClassHours", getTotalClassHours())
                .append("consumed", getConsumed())
                .append("residue", getResidue())
                .append("delFlag", getDelFlag())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("orderRemark", getOrderRemark())
                .append("orderRemark2", getOrderRemark2())
                .append("orderRemark3", getOrderRemark3())
                .append("orderRemark4", getOrderRemark4())
                .append("orderRemark5", getOrderRemark5())
                .append("orderRemark6", getOrderRemark6())
                .append("orderRemark7", getOrderRemark7())
                .append("orderRemark8", getOrderRemark8())
                .append("orderRemark9", getOrderRemark9())
                .append("orderRemark10", getOrderRemark10())
                .append("moneyDetermined", getMoneyDetermined())
                .append("moneyUnconfirmed", getMoneyUnconfirmed())
                .append("droppedClassesNumber", getDroppedClassesNumber())
                .append("moneyDroppedTotal", getMoneyDroppedTotal())
                .append("otherJson", getOtherJson())
                .append("moneyOtherTotal", getMoneyOtherTotal())
                .append("moneyOrderTotal", getMoneyOrderTotal())
                .append("profitAndLoss", getProfitAndLoss())
                .append("unitPrice", getUnitPrice())
                .append("unit", getUnit())
                .append("state", getState())
                .append("saleName", getSaleName())
                .append("deptId", getDeptId())
                .append("studentId", getStudentId())
                .append("saleId", getSaleId())
                .append("saleJson", getSaleJson())
                .toString();
    }
}
